# Secure Boot Library functions
**Header format**
```
tag_type - 1 byte - Specifies what is contained in the structure
length(in_bytes) - 1 byte - Specifies the length of the command
arguments(Function specific)
```

**Functions**

* *install_key(uint8 type_of_key,uint8 crypto_algo,uint8 pki_store_index,uint8 tgt_index,void *pointer_to_certificate)* - 

    * operation performed - The supplied certificate/table is verified and then the key is installed to the specified index in the key table.
    
    * returns if successful else initiates recovery    

* *auth_data(uint8 hash_algo, uint8 crypto_algo, uint8 pki_store_index,void *mem_addr_start, int length, void *pointer_to_signature)* -
    
    * operation performed - The precomputed hash is obtained by decrypting the signature using the key and algo specified. Data within mem_address_start and mem_address_end is hashed using the algorithm and compared with the precomputed hash. If it doesnt match, recovery mode is initiated.

    * returns if successful else initiates recovery    

* *dec_key(uint8 crypto_algo,uint8 mk_store_index,uint8 tgt_secret_store_index,void *pointer_to_blob)* -

    * operation performed - The encrypted blob is decrypted to obtain the key to decrypt the image using the master KEK at specified index.

    * returns always

* *dec_data(uint8 secret_store_index,void *pointer_to_encrypted_data, int length)* -
    
    * operation performed - Data within mem_address_start and mem_address_start+length is decrypted using the secret key at the specified index and the decrypted data is placed in the same place in memory.

    

**Headers and CSF Commands**
* CSF Header
    ```
        tag_type - 1 byte
        length - 2 bytes
        version - 1 byte
    ```

* Data Headers
    ```
        tag_type - 1 byte
        length - 1 byte
        data 
    ```

* SRK Table Header
    ```
        tag_type - 1 byte
        length - 2 byte
        number_of_srks - 1 byte
        key_n 
    ```

* Key Header
    ```
        key_type - 1 byte
        length - 2 bytes
        Key Algorithm - 1 byte
        Reserved(0000) - 2 bytes
        CA flag(80/00) - 1 byte
        public_key
    ```

* Secret Key Header
    ```
        crypt_algo - 1 byte
        Reserved(0000) - 2 bytes
        Hash Algorithm - 1 byte
        secret_key
        hash
    ```

* AES Key entry
    ```
        key_length - 2 bytes\
        iv_length - 2 bytes
        key 
        iv
    ```

* RSA Key entry
    ```
        modulus length - 2 bytes
        exponent length - 1 byte
        modulus
        exponent
    ```

* Install Key command
```
    fields in command - 
        tag_type - 1 byte
        length - 1 byte
        type_of_key - 1 byte
        crypto_algo - 1 byte
        index_of_key_for_verif - 1 byte
        tgt_key_installation_index - 1 byte
        offset_from_the_start_of_csf - 1 address word

    returns if successful else initiates recovery
    
    ```

* Authenticate Data command
```
    fields in command - 
        tag_type - 1 byte
        length - 1 byte
        hash_algo - 1 byte
        crypto_algo - 1 byte
        index_of_key_for_verif - 1 byte
        mem_address_start - 1 address word
        data_length - 1 address word
        offset_from_the_start_of_csf - 1 address word


    returns if successful else initiates recovery
```

* Decrypt Encryption Key
```
    fields in command - 
        tag_type - 1 byte
        length - 1 byte
        crypto_algo - 1 byte
        mk_key_index - 1 byte
        tgt_index - 1 byte
        pointer_to_encrypted_blob - 1 address word
```

* Decrypt Data
```
    fields in command - 
        tag_type - 1 byte
        length - 1 byte
        verif_index - 1 byte
        mem_address_start - 1 address word
        data_length - 1 address word    
```

**Tag Values and their meanings** 

| Value | Meaning                            |
|:-----:|------------------------------------|
| 0F    | CSF Header                         |
| 01    | SRK Table                          |
| 02    | Key Certificate                    |
| 03    | Signature                          |
| 04    | Public Key                         |
| 05    | Secret Key Entry                   |
| 10    | Operation - Install Key            |
| 11    | Operation - Authenticate Data      |
| 12    | Operation - Decrypt Encryption Key |
| 13    | Operation - Decrypt Data           |

