# Shakti Secure Boot Framework
 
## Introduction
Secure boot aims to protect computer systems during boot by detecting and preventing execution of modified binaries. This enforces the exclusive execution of authorized binaries in the computer system. The secure boot framework is  aimed at mitigating bootkit and rootkit attacks and  any other unauthorised firmware from running on the device. Additionally, similar authentication guarantees can be achieved at higher levels of software abstractions.
 
## Assumptions, Threat Model and Security Guarantees
### Threat Model
The threat model assumes that any binary that gets loaded and executed in a computer system, after the Boot ROM code, 
could potentially be tampered by an attacker. 

#### Security Guarantees
The secure boot solution guarantees that only untampered binaries will be loaded and executed by the processor, provided,
* the signature of the binary to execute after the boot ROM code is registered in the device and 
* for any subsequent binary that is loaded, the *sb_verify* API is invoked prior to the loading.

The first program to be loaded after the bootloader could either be an embedded standalone program on a bare-metal system configuration, 
or a first stage bootloader (FSBL) that in turn loads another bootloader, or a Operating System image. 
For any subsequent binary that is loaded, for instance by the Operating System or in a Virtual Machine, the secure boot solution uses
the *sb_verify* present in the  OS or VM loader (resp.) to perform the authentication before permitting the execution.

The library also provides API support for running encrypted executables on the device. 
 
### Assumptions
* We assume the Boot ROM code, i.e. the first code executed after the processor resets, is implicitly trusted. 
* Every loader in the program (for example in the OS or VM) implements the *sb_verify* API to authenticate of binaries. We assume these APIs are trusted.
* We cannot prevent malware that performs code injection attacks or code reuse attacks by exploiting bugs in software, like buffer overflows or format string vulnerabilities. 
* Any form of denial of service attacks, covert channels, or side-channel attacks are not a part of the threat model.
* The security guarantees rely on the cryptographic strength of the underlying hash functions and encryption algorithms used. For example, the one-wayness of hash functions and the cryptographic robustness of the public key algorithms. 

 
## Signing and Verifying Binaries

Secure boot requires that every program binary intended to be executed on the computer needs to be signed using cryptographic hash function and a public-key algorithm. This requires a private key Kr present in the host system which is used to sign binaries, and a corresponding public key Ku used by the processor to verify the signature. 

To sign a program binary P, the cryptographic hash of P is computed (using a hashing algorithm like SHA256) and the hash is encrypted using the public key Kr. Let this signature be Sr. This operation is done on the host system as a part of prepping the executable. 
At the time of loading the executable in the hardware, the binary Pe, along with the signature Sr is read by the processor. The processor verifies the signature by computing the hash (say, C1) of the program and also decrypts Sr using the public key Ku. It then matches this decrypted value with C1 and checks if they are equal. The equality implies that neither the program, nor the signature has been tampered with.

## Lexicon 
 
* #### Command Sequence File (CSF)
  The CSF using pseudo-instructions, specifies the various operations that needs to be performed when verifying the authenticity of a signed program, and also contains the various signatures that are required to verify the signed binary. These pseudo-instructions are parsed by the *sb_verify* API (that is defined in the BootROM) to perform the verification process befittingly. These pseudo-instructions can be installation of a new key, verification of CSF or verification of data/image. The API for the library is accessible at every level and can be employed accordingly by the software to provide security guarantees.
 
* #### Code Signing Tool (CST)
  The CST generates a signed binary on a host system. The signed binary comprises of the executable and the CSF. 
  The inputs for the CST include the following - 
    * Input CSF file(in ini format)
    * ELF executable / hexdump of the target program
  The output from the CST can be any of the following depending on the options specified - 
    * Modified ELF(with the binary csf embedded as a note section)
    * Binary file whose contents represent the csf in binary form

* #### Super Root Key (SRK)
  The SRK refers to the very first key in the PKI tree. This key acts as the root of trust for the complete system. SRKs are installed on hardware post manufacturing (possibly using e-fuses). Multiple SRKs are installed on the e-fuses for supporting revocation of an SRK. Also, in order to reduce the size of the fuses required, rather than storing the public keys of all SRKs (say SRK0, SRK1, ... SRKn), the following value is stored in the e-fuse:
 
      Fuse_value = Hash({SRK0, SRK1, ... , SRKn})
      
  where Hash indicates any hashing algorithm, and the curly braces indicate concatenation operation. A double hash is necessary to keep the length of the value constant and reduce the number of fuse bits needed to store the value.
  This *Fuse_value* is used during the boot process to verify the integrity of the SRKs supplied. The SRKs are placed in a data structure called the *SRK Table* and the hash is calculated as per the process specified above. This calculated value is verified against the *Fuse_value* stored in the device.
  Note that though multiple SRKs are installed, one boot processes makes use of only one SRK (which will be indicated in the CSF of FSBL)

* #### Public Key Infrastructure (PKI) Tree
  Different keys might need to be used to sign different softwares at various levels of hierarchy (like Bootloader, OS, User-application, etc.). In order to have a chain of trust, it is required to maintain a hierarchical key management system.
  For example, the SRK could be used to sign the Bootloader, and also a key (say K1). K1 can now be used to sign any other program (like the OS). At runtime, once SRK is installed and the bootloader is loaded, the CSF of the OS could contain a psuedo-instruction to install K1 (which is verified using the already installed SRK), followed by validation of the OS image(using K1). This hierarchical structure of keys is termed as the PKI tree.
Only the SRK, and some keys signed using SRKs, can be used to sign other keys (thereby restricting the depth of this tree).
  The PKI being referred to here is similar to the one defined in the context of cybersecurity. In both the cases, every certificate is signed by a parent certificate leading to the root of the tree, which is typically a trusted entity in case of cybersecurity. However, in case of secure boot, the root of trust stems from the hash of the SRK present in the fuses. Due to the physical constraints of the fuse technology, a value stored cannot be modified at any point. This fact ensures that the hash value cannot be tampered with. At the start of a boot process, an SRK is installed/selected. The device provides guarantees that once installed, the SRK cannot be changed until reset. Hence the root of trust for the trust chain starts with the SRK which is anchored to the fuse value.

![Sample PKI Tree](pki.png)

The above tree depicts one of the possible forms of the PKI tree. The number of children at each level is unrestricted. Although the number of SRKs is fixed at the time of closing the device(enabling the device to only boot via secure boot), any number of SignKeys can be added to the tree at any point in time. The keys are stored in a custom certificate format described in the Appendix.

Note: A custom certificate format is preferred over the traditional x509 certificate format so as to avoid having a x509 certificate parser as a part of the library, thereby keeping the library lightweight. Also, no additional advantage provided by the x509 certificates.

Currently, the cst supports only RSA Keys. Support for other algorithms will be added in the future.

The certificates can be generated using the following command for the cst.
``` bash
$ cst gen-hcrt --help
Usage: cst gen-hcrt [OPTIONS] [NAMES]...

  Generate HAB public key certificate for exisiting key pair.

Options:
  --help  Show this message and exit.
```

A new RSA key can be added to the PKI using the following command.
``` bash
$ cst -cf ./work/config.ini add-key rsa --help
Usage: cst add-key rsa [OPTIONS]

  RSA key.

Options:
  --name TEXT      Name to refer to in the Database.  [required]
  --ca TEXT        Name of the Certifying authority in the Database.
                   [required]

  --size INTEGER   Number of Bits in the RSA key.
  --org TEXT       Name of Organisation.
  --ca-flag TEXT   Use the certificate to sign other certificates.
  --years INTEGER  Number of years for validity of the certificate.
  --help           Show this message and exit.
```

* #### Installing a Signing Key
  The process of installing a key involves verifying the signature of the key, and once verified, updating the PKS table to store the public key. This installation process applies to all keys except the SRK.
  Every Bootloader will have a CSF that stores the entire SRK table. For installing the SRK, the BootROM computes Hash( {Hash(SRK0), Hash(SRK1), ... , Hash(SRKn)} ) and checks if this value is equal to that stored in the fuse.
  If the values are equal, then it installs the SRK (SRK*i* is installed where *i* is indicated in the CSF).

* #### Data Encryption Key
  A symmetric key which is used to encrypt the executable or parts of it. This key is included in the csf in a specific format described below.

* #### Master Key
  A symmetric key written onto a fuse on the device. This key is used for encrypting the data encryption key. The key is not accessible directly and can only be used to perform a decryption operation on data. The decrypted data is stored onto the secret key store if decryption is successful.

* #### Encrypted Blob
  The master key is used to encrypt the data encryption key (by using the cipher in any authenticated encryption mode preferably). If the master key is known, this operation can be performed on the host system. If the master key is not known, access(either physical or via a secure tunnel) to the device is required to perform the operation.

* #### Installing a secret key
  The encrypted blob is provided to the accelerator, which uses the master key to decrypt and extract the data encryption key. If the decryption is successful, the key is installed onto the secret key store(a data structure to store secret keys).

* #### Secret Key Store
  A hardware data structure to store secret keys. The keys are not accessible to the programs directly as plaintext. The programs can specify the index of the key to be used for decrypting the ciphertext and the corresponding key is used by the accelerator without revealing the key on the bus.

  Note: The actual mode/type of communication between the secret key store and the encryption/decryption engines depends on the implementation.

## Authentication Flow

The authentication flow comprises of two parts: 
 
### Host

The host machine is responsible for generating the signed binary image using the CST. CST accepts a binary executable along with the corresponding keys of the PKI. The CST can use any exisiting PKI which can be specified in a yaml input file. In case no prebuilt PKI exists, the CST can generate a new PKI based on user inputs, and use one of the keys to sign an image. As part of the signed binary, the host generates a CSF that consists of various pseudo instructions and signatures.


Without loss of generality, irrespective of a bootloader or a standalone program, we would hereby refer to the first program that needs to be executed on the target (after the BootROM) as FSBL. A typical CSF for an FSBL would consist of the following steps (in-order of execution on the host):
 
     1. Install SRK and SRK table in csf
     2. Install SignKey for FSBL and add SignKey to csf(Optional)
     3. Install Secret Key and add Encrypted blob to csf(Optional)
     4. Encrypt FSBL using the Data Encryption Key(Optional)
     5. Verify FSBL i.e generate signature for (encrypted) executable and append to csf
     6. Verify CSF i.e generate signature for the pseudo-instructions and append to the csf
 
** *explain briefly, each of the steps, to show continuity. Like where are u installing SRK? How are u verifying CSF with which keys?*
** *What happens if Verify CSF fails or Verify FSBL fails? Is your processor unusable?*
** *SRKs are one time installed by burning fuses... what happens if I want to change the bootloader*
** *what happens if there is an error when burning fuses?*

Since the SignKey of FSBL is always signed with SRK, the second step should be done after the first step. The second step can be skipped if a Fastboot method(all binaries are signed using SRK) is chosen for the system. After these pseudo instructions, the SRK table and the various signatures are added to the CSF.
 
Additionally, a typical signing process involves signing the CSF first (to ensure that CSF doesn't get tampered), and then signing the binary. If encryption of the executable is necessary, the tool proceeds to generate the encrypted blob if the master key is available. If the master key is not available, the input must contain the encrypted blob provisioned previously. This encryted blob is also added to the csf. The executable/parts of it are encrypted using the Data Encryption Key. If encryption is enabled, the cipher is used to generate the signatures for authentication and the elf is modified accordingly too else the elf is used as provided. In case the binary contains multiple discontinuous sections, depending on the mode chosen, the tool will generate multiple signatures or a single signature for various parts of the program and modify the CSF to indicate these regions (so that these regions can be verified separately). These signatures are then stowed in the CSF file. which marks the completion of CSF generation. Once the CSF is generated, the tool adds the CSF to a custom section of the executable program.

The cst tool is used for generating the csf.
``` bash
$ cst gen-csf --help
Usage: cst gen-csf [OPTIONS]

  Generate CSF binary.

Options:
  --csf-file PATH  Path to the csf.ini file  [required]
  --help           Show this message and exit.
```
 
### Target
A typical CSF for an FSBL would consist of the following pseudo instructions (in-order of execution on the target):
 
     1. Install SRK
     2. Install SignKey for FSBL(Optional)
     3. Verify CSF
     4. Verify FSBL
     5. Install Secret Key(Optional)
     6. Decrypt FSBL(Optional)
     7. Jump to FSBL

![Authentication Flow for First Image in Boot Chain](authimage1.png)

The target (i.e. the RISC-V processor) is responsible for validating the signed binary FSBL image that was produced by the CST. The target, on reset, begins executing the BootROM code which looks for pseudo instructions of CSF. Once the SRK is installed, and the CSF is verified, the bootrom proceeds to verify the image as instructed in the CSF. If any of these checks fail, the processor halts*; else, the boot process is deemed to be a success. If specified in the csf, the bootrom then proceeds to install the secret key and then decrypt the executable. Once decryption is successful, the processor starts executing the FSBL code.
 
    *NOTE: In case any of the steps during authentication fails, the hardware first checks if the device has been closed(i.e configured to boot only in secure mode) using a special fuse location. If the fuse has been burnt, then the processor goes into a "halt mode"; if not, then the BootROM prints that the secure boot failed and proceeds to execute the FSBL code normally.

The authentication flow for other images is similar to the flow described above other than the fact that there is no install SRK command.

![Authentication Flow for other Images](authimage2.png)


## Key revocation
In case one compromises one of the SRK private keys, or if one needs to transfer the ownership, the SRK being used might need to be revoked so that images/CSFs signed using the old SRK no longer work on the target. If the SRK table has N keys, N single-bit fuses are provided to allow for revocation of the SRKs. A SRK key installation is successful during the boot only if the corresponding revocation fuse has not been blown. The revocation fuses can be blown by the software (FSBL CSF and BootROM). To blow the fuse *j* corresponding to SRK*j*, the FSBL CSF with the "blow-fuse" pseudo instruction should be signed with SRK*j*.

## Implementation possibilities
### Variant 1(current)
**Sailent features**
* Library present in the BootRom.
* No secure RAM.
* Two hardware enforced sticky entries in the PKS which is present as a separate TCM.
* PKS contains only 3 entries. Two are sticky and the other is temporary.
* The writes to the TCM are restricted to the library in ROM.
* E-Fuses for hash storage.
* Stack space of the caller program is used by the API with interrupts disabled.

**Caveats**
* The threat model also assumes that if the OS or hypervisor, or the  want to verify the signature of other program binaries which have been signed using a key that is not one of the 2 keys in the PKS, they should maintain their own PKS table. This helps extend the software stack at the cost of security guarantees. 
* The relevant parts of the program has to be present in the memory before the API can validate.
* No provision to handle page faults during authentication.
* Page wise authentication can be supported.
* Updates to library and hash are not possible.

## Variant 2
**Sailent features**
* Library present in the BootRom.
* Secure RAM is used for PKS storage.
* The writes to the TCM are restricted to the library in ROM.
* E-Fuses for hash storage.
* Stack space of the caller program is used by the API with interrupts disabled.

**Caveats**
* The relevant parts of the program has to be present in the memory before the API can validate.
* No provision to handle page faults during authentication.
* Page wise authentication can be supported.
* Updates to library and hash are not possible.

## Variant 3
**Sailent features**
* Library present in the BootRom.
* Secure RAM is used for PKS storage and as stack space.
* The writes to the TCM are restricted to the library in ROM.
* E-Fuses for hash storage.

**Caveats**
* Page faults can be handled during authentication enabling unconstrained authentication of programs.
* Page wise authentication can be supported.
* Updates to library and hash are not possible.

## Variant 3
**Sailent features**
* Library present in a secure persistent memory.
* Secure RAM is used for PKS storage and as stack space.
* The writes to the TCM are restricted to the library in ROM.
* Secure persistent memory for hash storage and platform key.

**Caveats**
* Page faults can be handled during authentication enabling unconstrained authentication of programs.
* Page wise authentication can be supported.
* Device is not bricked in case all SRKs are compromised.
* Updates to library and hash possible using the platform key.

# Appendix

## Blocks in Input file to tool

Format - ini

* Install SRK
``` ini
[Install SRK]
# Names of the keys in the database to be included in the SRK table 
Name = srk0 srk1 srk2 srk3
# The corresponding fuse file for the Table
Fuse = ./work/fuse.bin
# The index of the key in the SRK table which has to be installed.
Source index = 1
```

* Install SignKey
``` ini
[Install Key]
# Key slot index used to authenticate the key to be installed
#   0 - SRK
#   1 - OS Key
Verification index = 0
# Target key slot in HAB key store where key will be installed
Target Index = 2
# Name of the key to install
Name = signk1.1
```

* Authenticate Data
``` ini
[Authenticate Data]
# Key slot index used to authenticate the image data
Verification index = 2

# For ELF files
type = elf
# Modify the elf with csf as output
modify = True
# Load the csf into memory. Typically required only for FSBL
load = True
# VA for the csf section. Necessary only if load = True
address = 0x80005000
# Name of the output file
output = hello.elf 
# The path to the elf file
file = ../lib/output/hello.riscv

# For elf2hex outputs
#type = hex
## Name of the output file
#output = csf.bin
## Number of bytes in a single line
#num_bytes = 1
## Path to the input file
#file = ../lib/output/code.mem

Blocks = 0x80000000 0x000 0x00001
```


## CSF Commands

* Install Key command
```
    fields in command - 
        tag_type - 1 byte
        length - 1 byte
        type_of_key - 1 byte
        crypto_algo - 1 byte
        index_of_key_for_verif - 1 byte
        tgt_key_installation_index - 1 byte
        offset_from_the_start_of_csf - 1 address word

    returns if successful else initiates recovery
    
```

* Authenticate Data command
```
    fields in command - 
        tag_type - 1 byte
        length - 1 byte
        hash_algo - 1 byte
        crypto_algo - 1 byte
        index_of_key_for_verif - 1 byte
        mem_address_start - 1 address word
        data_length - 1 address word
        offset_from_the_start_of_csf - 1 address word


    returns if successful else initiates recovery
```

* Decrypt Encryption Key
```
    fields in command - 
        tag_type - 1 byte
        length - 1 byte
        crypto_algo - 1 byte
        mk_key_index - 1 byte
        tgt_index - 1 byte
        pointer_to_encrypted_blob - 1 address word
```

* Decrypt Data
```
    fields in command - 
        tag_type - 1 byte
        length - 1 byte
        verif_index - 1 byte
        mem_address_start - 1 address word
        data_length - 1 address word    
```

## Data Structures 

* CSF Header
``` C
    uint8_t tag_type;   // 0x0f - For csf
    uint16_t length;
    uint8_t version;    // Version of Shakti Secure Boot
```

* SRK Table Header
``` C
    uint8_t tag_type;        // 0x01 - For SRK Table
    uint16_t length;   
    uint8_t num_of_srks; 
    uint8_t *entries;        // Array of size num_of_srks. Each entry holding a pointer of type Public_key_Entry
```

* Certificate format
``` C
    uint8_t tag_type;        // 0x02 - For Certificate
    uint16_t cert_length;
    Public_Key_Header;
    Public_Key_entry;
    uint8_t tag_type;        // 0x03 - For signature
    uint16_t sig_length;
    uint8_t *signature;      // Array of size sig_length
```

* Public Key Header
``` C
    uint8_t tag_type;           // 0x04 - For Public Key
    uint16_t entry_length; 
    uint8_t key_algo;
    uint16_t Reserved;          // 0 - Reserved Bytes
    uint8_t ca_flag - 0x80/0x00 // Denoting whether the CA flag is set in the certificate
```

* Public Key Entry for RSA
``` C
    uint16_t modulus_length;
    uint16_t exponent_length;
    uint16_t rr_length;  
    // Modulus value of the RSA Key
    uint8_t *modulus;             // Array of size modulus_length
    // Exponent value of the RSA Key
    uint8_t *exponent;            // Array of size exponent_length
    /* RR value as used in the Mongomery Modular Exponentation algorithm
    rr = (2^(2 * modulus_length)) % modulus
    */
    uint8_t *rr;                  // Array of size rr_length
```

## Tag Value Table 

| Value | Meaning                            |
|:-----:|------------------------------------|
| 0F    | CSF Header                         |
| 01    | SRK Table                          |
| 02    | Key Certificate                    |
| 03    | Signature                          |
| 04    | Public Key                         |
| 10    | Operation - Install Key            |
| 11    | Operation - Authenticate Data      |
| 12    | Operation - Decrypt Encryption Key |
| 13    | Operation - Decrypt Data           |



