# Shakti Secure Boot Framework
 
## Introduction
Secure boot aims to protect computer systems during boot by detecting and preventing execution of modified binaries. This enforces the exclusive execution of authorized binaries in the computer system. The secure boot framework is  aimed at mitigating bootkit and rootkit attacks and  any other unauthorised firmware from running on the device. Additionally, similar authentication guarantees can be achieved at higher levels of software abstractions.
 
## Assumptions, Threat Model and Security Guarantees
### Threat Model
The threat model assumes that any binary that gets loaded and executed in a computer system, after the Boot ROM code, 
could potentially be tampered by an attacker. 

#### Security Guarantees
The secure boot solution guarantees that only untampered binaries will be loaded and executed by the processor, provided,
* the signature of the binary to execute after the boot ROM code is registered in the device and 
* for any subsequent binary that is loaded, the *sb_verify* API is invoked prior to the loading.

The first program to be loaded after the bootloader could either be an embedded standalone program on a bare-metal system configuration, 
or a first stage bootloader (FSBL) that in turn loads another bootloader, or a Operating System image. 
For any subsequent binary that is loaded, for instance by the Operating System or in a Virtual Machine, the secure boot solution uses
the *sb_verify* present in the  OS or VM loader (resp.) to perform the authentication before permitting the execution.

The library also provides API support for running encrypted executables on the device. 
 
### Assumptions
* We assume the Boot ROM code, i.e. the first code executed after the processor resets, is implicitly trusted. 
* Every loader in the program (for example in the OS or VM) implements the *sb_verify* API to authenticate of binaries. We assume these APIs are trusted.
* We cannot prevent malware that performs code injection attacks or code reuse attacks by exploiting bugs in software, like buffer overflows or format string vulnerabilities. 
* Any form of denial of service attacks, covert channels, or side-channel attacks are not a part of the threat model.
* The security guarantees rely on the cryptographic strength of the underlying hash functions and encryption algorithms used. For example, the one-wayness of hash functions and the cryptographic robustness of the public key algorithms. 

 
## Signing and Verifying Binaries

Secure boot requires that every program binary intended to be executed on the computer needs to be signed using cryptographic hash function and a public-key algorithm. This requires a private key Kr present in the host system which is used to sign binaries, and a corresponding public key Ku used by the processor to verify the signature. 

To sign a program binary P, the cryptographic hash of P is computed (using a hashing algorithm like SHA256) and the hash is encrypted using the private key Kr. Let this signature be Sr. This operation is done on the host system as a part of prepping the executable. 
At the time of loading the executable in the hardware, the binary Pe, along with the signature Sr is read by the processor. The processor verifies the signature by computing the hash (say, C1) of the program and also decrypts Sr using the public key Ku. It then matches this decrypted value with C1 and checks if they are equal. The equality implies that neither the program, nor the signature has been tampered with.

## Lexicon 
 
* #### Command Sequence File (CSF)
  The CSF using pseudo-instructions, specifies the various operations that needs to be performed when verifying the authenticity of a signed program, and also contains the various signatures that are required to verify the signed binary. These pseudo-instructions are parsed by the *sb_verify* API (that is defined in the BootROM) to perform the verification process befittingly. These pseudo-instructions can be installation of a new key, verification of CSF or verification of data/image. The API for the library is accessible at every level and can be employed accordingly by the software to provide security guarantees.
 
* #### Code Signing Tool (CST)
  The CST generates a signed binary on a host system. The signed binary comprises of the executable and the CSF. 
  The inputs for the CST include the following - 
    * Input CSF file(in ini format)
    * ELF executable / hexdump of the target program
  The output from the CST can be any of the following depending on the options specified - 
    * Modified ELF(with the binary csf embedded as a note section)
    * Binary file whose contents represent the csf in binary form

* #### Super Root Key (SRK)
  The SRK refers to the very first key in the PKI tree. This key acts as the root of trust for the complete system. SRKs are installed on hardware post manufacturing (possibly using e-fuses). Multiple SRKs are installed on the e-fuses for supporting revocation of an SRK. Also, in order to reduce the size of the fuses required, rather than storing the public keys of all SRKs (say SRK0, SRK1, ... SRKn), the following value is stored in the e-fuse:
 
      Fuse_value = Hash({SRK0, SRK1, ... , SRKn})
      
  where Hash indicates any hashing algorithm, and the curly braces indicate concatenation operation. A double hash is necessary to keep the length of the value constant and reduce the number of fuse bits needed to store the value.
  This *Fuse_value* is used during the boot process to verify the integrity of the SRKs supplied. The SRKs are placed in a data structure called the *SRK Table* and the hash is calculated as per the process specified above. This calculated value is verified against the *Fuse_value* stored in the device.
  Note that though multiple SRKs are installed, one boot processes makes use of only one SRK (which will be indicated in the CSF of FSBL)

* #### Signature of data
  The hash of the data is calculated and encrypted using a key(private key in case of asymmetric algorithms). This value is called the signature of the data. 

* #### Verifying a Signature
  To verify a signature, the data, along with a key (public key in case of asymmetric algorithms) and the signature are required. Verifying the signature of a data involves the following steps.
    * Calculate the hash(*h_a*) of the data.
    * Decrypt the signature using the key to obtain *h_s*.
    * Check if *h_a* and *h_s* are equal. The verification is said to be successful if both the values are equal.

* #### Public Key Infrastructure (PKI) Tree
  Different keys might need to be used to sign different softwares at various levels of hierarchy (like Bootloader, OS, User-application, etc.). In order to have a chain of trust, it is required to maintain a hierarchical key management system.
  For example, the SRK could be used to sign the Bootloader, and also a key (say K1). K1 can now be used to sign any other program (like the OS). At runtime, once SRK is installed and the bootloader is loaded, the CSF of the OS could contain a psuedo-instruction to install K1 (which is verified using the already installed SRK), followed by validation of the OS image(using K1). This hierarchical structure of keys is termed as the PKI tree.
Only the SRK, and some keys signed using SRKs, can be used to sign other keys (thereby restricting the depth of this tree).
  The PKI being referred to here is similar to the one defined in the context of cybersecurity. In both the cases, every certificate is signed by a parent certificate leading to the root of the tree, which is typically a trusted entity in case of cybersecurity. However, in case of secure boot, the root of trust stems from the hash of the SRK present in the fuses. Due to the physical constraints of the fuse technology, a value stored cannot be modified at any point. This fact ensures that the hash value cannot be tampered with. At the start of a boot process, an SRK is installed/selected. The device provides guarantees that once installed, the SRK cannot be changed until reset. Hence the root of trust for the trust chain starts with the SRK which is anchored to the fuse value.

* #### Data Encryption Key
  A symmetric key which is used to encrypt the executable or parts of it. This key is included in the csf in a specific format described below.

* #### Master Key
  A symmetric key written onto a fuse on the device. This key is used for encrypting the data encryption key. The key is not accessible directly and can only be used to perform a decryption operation on data. The decrypted data is stored onto the secret key store if decryption is successful.

* #### Encrypted Blob
  The master key is used to encrypt the data encryption key (by using the cipher in any authenticated encryption mode preferably). If the master key is known, this operation can be performed on the host system. If the master key is not known, access(either physical or via a secure tunnel) to the device is required to perform the operation. The format of the encrypted blob is as follows:

![Encrypted Blob](graphics/ds_encrypted_blob.png)

* #### Secret Key Store
  A hardware data structure to store secret keys. The keys are not accessible to the programs directly as plaintext. The programs can specify the index of the key to be used for decrypting the ciphertext and the corresponding key is used by the accelerator without revealing the key on the bus.

  Note: The actual mode/type of communication between the secret key store and the encryption/decryption engines depends on the implementation. Also, this data structure is volatile and loses all information on a reset/power-off.

* #### Public Key Store
  A data structure to store trusted public keys at runtime. The keys for a signature verification are always obtained from this data structure.

  Note: This data structure is volatile and loses all information on a reset/power-off.



## Authentication Flow

The authentication flow comprises of two parts, the host (where the CSF and the signed image is generated) and the target (which verifies the signed image). In order to describe the flow, we first define the following keywords that would be used:

1. SRK: The Super Root Key, or SRK, refers to the public key of the master key pair. The private key (of the master key pair) is used to sign a key/image, and the public key (of the master key pair) is used to verify the signature.
2. SignKey: This refers to the public key of the FSBL key pair, i.e., the key pair used to sign and verify the FSBL.
3. Master Private Key: This refers to the symmetric key that are burned onto the Master private key fuses in the hardware. These are used to provision the Secret Keys.
4. SecretKey: This refers to the symmetric key that is used to encrypt any program that is being loaded. In this particular example, we assume that the SecretKey is used to encrypt the FSBL.
 
### Pseudo-Commands and the operations performed
Following are the set of pseudo commands that can be specified in a CSF file.

#### Install SRK
This pseudo-command instructs the device to authenticate a Super Root Key and is mandatory only for the csf of the FSBL. The contents
of various keys are identified from the table and the hash is calculated using the equation specified previously.
This value is compared with the value stored on the Super Root Key Fuse. Once the
value matches, checks are made to verify if the particular SRK has been revoked (by
reading the value of SRK revoke fuse). If unrevoked, the specified SRK is installed onto
the public key store and marked as trusted; else, the assured boot process fails.

Note: Only one SRK can be specified during a boot sequence. Once an SRK has been
marked as trusted and installed onto the public key store, it cannot be modified or changed.
Only a reset of the system can facilitate a new SRK installation. Thus, only the csf of the FSBL can contain an *Install SRK* pseudo command.

![Install SRK](graphics/flo_install_srk.png)

#### Install Sign Key 
This pseudo-command instructs the device to authenticate a Sign Key. This key can be used to verify the signature of the program(s) that would be loaded and executed on the processor. To mark the key as trusted and install it in the PKS, we verify the signature of the key using the SRK that is installed.

![Install Sign Key](graphics/flo_install_signkey.png)

#### Authenticate CSF

This pseudo-command instructs the device to authenticate the source of CSF. This is performed by verifying the signature of the CSF using a key from the PKS. This is a mandatory pseudo-command in every CSF, and any of the subsequent psuedo-commands are permitted only after successful execution of this step.

![Authenticate CSF](graphics/flo_auth_csf.png)

#### Authenticate Data
This pseudo-command instructs the device to authenticate the image. The signature
verification for the data/image is carried out in a similar manner to the signature verification
of the CSF. This is a mandatory pseudo-command in every CSF.

![Authenticate Data](graphics/flo_auth_image.png)

#### Install Secret Key
This pseudo-command is only allowed after a verify CSF command has been successfully
executed. Any attempt to call this command otherwise results in a failure. If the CSF has
been marked trusted, the signature and the ciphertext are retrieved from the encrypted
blob. The signature is decrypted using the master key at the specified index and then
compared with a freshly calculated hash of the ciphertext. If they match, the ciphertext
is again decrypted using the master key and installed onto the secret key store.

![Install Secret Key](graphics/flo_install_secretkey.png)

#### Decrypt Data
This pseudo-command is only allowed after a *Authenticate CSF* has been successfully
executed.
If the CSF has been marked trusted, the key at the specified index in the secret key store is used by the accelerator to decrypt ciphertext in a memory buffer.
The buffer is overwritten with the plaintext.

### Host

The host machine is responsible for generating the signed binary image using the CST. CST accepts a binary executable along with an optional set of keys from the PKI. In case no prebuilt PKI exists, the CST can generate a new PKI (based on user inputs), and use one of the keys to sign an image. As part of the signed binary, the host generates a CSF that consists of various pseudo instructions and signatures.
A typical CSF for an FSBL would consist of the steps mentioned below. Note that the inclusion of various items in the CSF will NOT be in the order that is mentioned below. For example, the CSF signature can be computed only after all CSF psuedo instructions have been generated, and hence is computed at the end. However, the psuedo instruction to verify the CSF should be present in the beginning so that the target first verifies the CSF before executing various psuedo instructions. Therefore, the steps mentioned below are in the order in which computations are performed on the host.
 
     1.  Generate PKI (in case it is not provided as an input).
     2.  Compute the signature of the SRK.
     3.  Include the SRK Table, along with the signature in the final image.
     4.  Add a psuedo instruction for installing the SRK in the CSF.
     5.  Sign the SignKey (using SRK) and include the signature in the final image.
     6.  Add a psuedo instruction to install the SignKey in the CSF.
     7.  Encrypt the SecretKey (using the Master private key), and, include the encrypted SecretKey and a psuedo instruction to install the Secret Key in the CSF.
     8.  Encrypt FSBL using the SecretKey, and include the encrypted blob in the final image.
     9.  Compute the signature of the encrypted blob (using the private key of the FSBL key pair) and include it in the final image.
     10. Add a psuedo instruction to verify (using the SignKey) if the signature of the encrypted blob (which was included in step 8) matches the stored value (which was included in step 9).
     11. Compute the signature of the CSF and include it in the final image.
     12. Add an instruction to verify the CSF.

A typical signing process involves signing the CSF first (to ensure that CSF doesn’t get
tampered). If encryption of the executable is necessary, the tool proceeds to generate
the encrypted blob if the master key is available. If the master key is not available, the
input must contain the encrypted blob provisioned previously. This encrypted blob is also
added to the csf. The executable/parts of it are encrypted using the Data Encryption Key.
If encryption is enabled, the cipher is used to generate the signatures for authentication
and the ELF is modified accordingly too, else the ELF is used as provided. In case the
binary contains multiple discontinuous sections, depending on the mode chosen, the tool
will generate multiple signatures or a single signature for various parts of the program and
modify the CSF to indicate these regions (so that these regions can be verified separately).
These signatures are then stowed in the CSF file, which marks the completion of CSF
generation. Once the CSF is generated, the tool adds the CSF to a custom section of the
executable program. 
<!-- ** *explain briefly, each of the steps, to show continuity. Like where are u installing SRK? How are u verifying CSF with which keys?*
** *What happens if Verify CSF fails or Verify FSBL fails? Is your processor unusable?*
** *SRKs are one time installed by burning fuses... what happens if I want to change the bootloader*
** *what happens if there is an error when burning fuses?*

Since the SignKey of FSBL is always signed with SRK, the second step should be done after the first step. The second step can be skipped if a Fastboot method(all binaries are signed using SRK) is chosen for the system. After these pseudo instructions, the SRK table and the various signatures are added to the CSF.
 
Additionally, a typical signing process involves signing the CSF first (to ensure that CSF doesn't get tampered), and then signing the binary. If encryption of the executable is necessary, the tool proceeds to generate the encrypted blob if the master key is available. If the master key is not available, the input must contain the encrypted blob provisioned previously. This encryted blob is also added to the csf. The executable/parts of it are encrypted using the Data Encryption Key. If encryption is enabled, the cipher is used to generate the signatures for authentication and the elf is modified accordingly too else the elf is used as provided. In case the binary contains multiple discontinuous sections, depending on the mode chosen, the tool will generate multiple signatures or a single signature for various parts of the program and modify the CSF to indicate these regions (so that these regions can be verified separately). These signatures are then stowed in the CSF file. which marks the completion of CSF generation. Once the CSF is generated, the tool adds the CSF to a custom section of the executable program. -->

### Target
A typical CSF for an FSBL would consist of the following pseudo instructions in the order specified below. This is the order of execution on the target (i.e. the RISC-V processor):
 
     1. Install SRK
     2. Install SignKey for FSBL
     3. Verify CSF
     4. Verify FSBL
     5. Install Secret Key
     6. Decrypt FSBL
     7. Jump to FSBL

The target is responsible for validating the signed binary FSBL image that was produced by the CST. The target, on reset, begins executing the BootROM code which looks for pseudo instructions of CSF. Once the SRK is installed, and the CSF is verified, the bootrom proceeds to verify the image as instructed in the CSF. If any of these checks fail, the processor halts*; else, the boot process is deemed to be a success. If specified in the csf, the bootrom then proceeds to install the secret key and then decrypt the executable. Once decryption is successful, the processor starts executing the FSBL code.
 
    *NOTE: In case any of the steps during authentication fails, the hardware first checks if the device has been closed(i.e configured to boot only in secure mode) using a special fuse location. If the fuse has been burnt, then the processor goes into a "halt mode"; if not, then the BootROM prints that the secure boot failed and proceeds to execute the FSBL code normally.


## Secret Key Operation

### Generating the Encrypted Blob
The encrypted blob can be generated on the host system if the master key is available, else the device can be used to provision the encrypted blob. The steps to generate are as follows:
    1. Encrypt the DEK using the Master Key to generate the *dek_cipher*.
    2. Calculate the hash(*hash_dc*) of *dek_cipher*.
    3. Encrypt *hash_dc* using the same algorithm, mode and IV as used in step 1 to generate *cipher_hdc*.
    4. Generate the blob by concactenating the required data and headers.

### Installing the Secret Key
The Install Secret Key operation can be performed on the target device only after the verification of the csf signature. This ensures that the access to the secret key store is allowed only to the trusted entities. The operation consists of the follwing steps:
    1. Decrypt the Signature(*cipher_hdc*) using the master key to extract the hash.
    2. Calculate the hash of the encrypted dek(*dek_cipher*) and verify against value calculated in step 1.
    3. On successful verification, decrypt the encrypted dek(*dek_cipher*) and write the dek onto the secret key store.

## Key revocation
In case one compromises one of the SRK private keys, or if one needs to transfer the ownership, the SRK being used might need to be revoked so that images/CSFs signed using the old SRK no longer work on the target. If the SRK table has N keys, N single-bit fuses are provided to allow for revocation of the SRKs. A SRK key installation is successful during the boot only if the corresponding revocation fuse has not been blown. The revocation fuses can be blown by the software (FSBL CSF and BootROM). To blow the fuse *j* corresponding to SRK*j*, the FSBL CSF with the "blow-fuse" pseudo instruction should be signed with SRK*j*.

## Implementation possibilities
### Variant 1(current)
**Sailent features**
* Library present in the BootRom.
* No secure RAM.
* Two hardware enforced sticky entries in the PKS which is present as a separate TCM.
* PKS contains only 3 entries. Two are sticky and the other is temporary.
* The writes to the TCM are restricted to the library in ROM.
* E-Fuses for hash storage.
* Stack space of the caller program is used by the API with interrupts disabled.

**Caveats**
* The threat model also assumes that if the OS or hypervisor want to verify the signature of other program binaries which have been signed using a key that is not one of the 2 keys in the PKS, they should maintain their own PKS table. This helps extend the software stack at the cost of security guarantees. 
* The relevant parts of the program has to be present in the memory before the API can validate.
* No provision to handle page faults during authentication.
* Page wise authentication can be supported.
* Updates to library and hash are not possible.

### Variant 2
**Sailent features**
* Library present in the BootRom.
* Secure RAM is used for PKS storage.
* The writes to the TCM are restricted to the library in ROM.
* E-Fuses for hash storage.
* Stack space of the caller program is used by the API with interrupts disabled.

**Caveats**
* The relevant parts of the program has to be present in the memory before the API can validate.
* No provision to handle page faults during authentication.
* Page wise authentication can be supported.
* Updates to library and hash are not possible.

### Variant 3
**Sailent features**
* Library present in the BootRom.
* Secure RAM is used for PKS storage and as stack space.
* The writes to the TCM are restricted to the library in ROM.
* E-Fuses for hash storage.

**Caveats**
* Page faults can be handled during authentication enabling unconstrained authentication of programs.
* Page wise authentication can be supported.
* Updates to library and hash are not possible.

### Variant 3
**Sailent features**
* Library present in a secure persistent memory.
* Secure RAM is used for PKS storage and as stack space.
* The writes to the TCM are restricted to the library in ROM.
* Secure persistent memory for hash storage and platform key.

**Caveats**
* Page faults can be handled during authentication enabling unconstrained authentication of programs.
* Page wise authentication can be supported.
* Device is not bricked in case all SRKs are compromised.
* Updates to library and hash possible using the platform key.

# Signing Tool CSF Alignment Constraints and Rationale
* Encryption Block Size - The size of the block for encryption should always be a multiple of 128 bytes. This is due to the limitations of the elf file structure, the parser and the loader. The block is parsed out of the elf, encrypted and the block data in the elf is replaced with the ciphertext. Since no headers are modified in the signing tool, the tool cannot modify the sizes of the sections/segments, which is a necessary feature for handling varied block sizes(padding is necessary for
  the AES algorithm).  

* Encryption Block Consecutivity - A single block can only contain condiguous data. This doesn't restrict encrypting non-condiguous data, this effectively forces non-condiguous data encryption to be split into multiple condiguous data encryption operations.

# Programmer's guide to use the library

## Parsing the CSF
A default CSF parser is provided as a header file(*csf_parser.h*). The header file contains a function to parse and perform all the operations specified in the csf. This function can be used to authenticate programs and establish trust. The function prototype is listed below:
```
    int validate_program(void *csf_addr)
```

An implementation may choose to use the provided csf parser or use a custom csf parser(to add additional capabilities/custom operations). The various data structures used in the csf are highlighted in the upcoming section.

## Calling the Library functions
The *secvt* is a pointer to the shakti secure library vector table. This table consists of 8 entries. Each entry in the table is a pointer to a secure library function. The header file *seclib.h* consisits of macros which call these functions using the appropriate pointer and the specified arguments. Each macro takes a pointer to the *secvt* as argument. The macros and their functions are as follows:

* ALLOC_CTX(secvt)

    This macro calls the function to allocate a secure context for use during authentication. The function takes no arguments and returns a value of type *sec_id*. This value is passed as the first argument in all the authentication and installation functions.

* INSTALL_KEY(secvt)

    This macro calls the function to install a public key into the public key store using the process specified above. The arguments to the function and their types are listed below:
    * sec_id id
    * uint8_t type_of_key
    * uint8_t crypto_algo
    * uint8_t source_index
    * uint8_t tgt_index
    * void *pointer_to_certificate

    The function returns a value of 1 if the operation was successful, else a negative error code is returned.

* AUTHENTICATE_CSF(secvt)
    
    This macro calls the function to authenticate the csf using the procedure mentioned above. The arguments to the function and their types are listed below:
    * sec_id id
    * uint8_t hash_algo
    * uint8_t crypto_algo
    * uint8_t pki_store_index
    * void *blocks
    * int num
    * void *pointer_to_signature
    
    The function returns a value of 1 if the operation was successful, else a negative error code is returned.

* AUTHENTICATE_DATA(secvt)
    
    This macro calls the function to authenticate image data using the procedure mentioned above. The arguments to the function and their types are listed below:
    * sec_id id
    * uint8_t hash_algo
    * uint8_t crypto_algo
    * uint8_t pki_store_index
    * void *blocks
    * int num
    * void *pointer_to_signature
    
    The function returns a value of 1 if the operation was successful, else a negative error code is returned.

* DECRYPT_DATA(secvt)

    This macro calls the function to decrypt image data using the procedure mentioned above. The arguments to the function and their types are listed below:
    * sec_id id
    * uint8_t crypto_algo
    * uint8_t mode
    * uint8_t index
    * void *blocks
    * int num
    * void *iv    

    The function returns a value of 1 if the operation was successful, else a negative error code is returned.

* REVOKE_SK(secvt)

    This macro calls the function to revoke a secret key from the secret key store. The arguments to the function and their types are listed below:
    * sec_id id
    * uint8_t index
    
    The function returns a value of 1 if the operation was successful, else a negative error code is returned.

* REVOKE_PK(secvt)

    This macro calls the function to revoke a public key from the public key store. The arguments to the function and their types are listed below:
    * sec_id id
    * uint8_t index
    
    The function returns a value of 1 if the operation was successful, else a negative error code is returned.

* FREE_CTX(secvt)

    This macro calls a function to destroy the shakti secure library contex allocated using the first function.
    The argument to the function are as follows:
    * sec_id id

    The function returns a value of 1 if the operation was successful, else a negative error code is returned.

## Data Structures

** Note: All pointers are relative to the start of the csf.**

* CSF Header
    ```
        tag_type - 1 byte
        length - 2 bytes
        csf_version - 1 byte
        prog_version - 1 byte
    ```

* Data Headers
    ```
        tag_type - 1 byte
        length - 1 byte
        data 
    ```

* SRK Table Header
    ```
        tag_type - 1 byte
        length - 2 byte
        number_of_srks - 1 byte
        key * n 
    ```

* Key Header
    ```
        key_type - 1 byte
        length - 2 bytes
        Key Algorithm - 1 byte
        Reserved(0000) - 2 bytes
        CA flag(80/00) - 1 byte
        public_key
    ```

* Secret Key Header
    ```
        crypt_algo - 1 byte
        Reserved(0000) - 2 bytes
        Hash Algorithm - 1 byte
        secret_key
        hash
    ```

* AES Key entry
    ```
        key_length - 2 bytes\
        iv_length - 2 bytes
        key 
        iv
    ```

* RSA Key entry
    ```
        modulus_length - 2 bytes
        exponent_length - 1 byte
        modulus
        exponent
    ```

* Block Descriptor(block_desc)
    ```
        start_address - 1 address word
        length - 1 address word
    ```

* Install Key command
    ```
        tag_type - 1 byte
        length - 1 byte
        type_of_key - 1 byte
        crypto_algo - 1 byte
        index_of_key_for_verif - 1 byte
        tgt_key_installation_index - 1 byte
        pointer_to_signature - 1 address word
    ```

* Authenticate Data command
    ```
        tag_type - 1 byte
        length - 1 byte
        hash_algo - 1 byte
        crypto_algo - 1 byte
        index_of_key_for_verif - 1 byte
        pointer_to_signature - 1 address word
        block_desc - number of blocks * 2 address words 
    ```

* Decrypt Encryption Key
    ```
        tag_type - 1 byte
        length - 1 byte
        crypto_algo - 1 byte
        mk_key_index - 1 byte
        tgt_index - 1 byte
        pointer_to_encrypted_blob - 1 address word
    ```

* Decrypt Data
    ```
        tag_type - 1 byte
        length - 1 byte
        crypto_algo - 1 byte
        mode - 1 byte
        verif_index - 1 byte
        pointer_to_iv - 1 address word
        block_desc - number of blocks * 2 address words
    ```

* Tag Values and their meanings

    | Value | Meaning                            |
    |:-----:|------------------------------------|
    | 0F    | CSF Header                         |
    | 01    | SRK Table                          |
    | 02    | Key Certificate                    |
    | 03    | Signature                          |
    | 04    | Public Key                         |
    | 05    | Secret Key Entry                   |
    | 10    | Operation - Install Key            |
    | 11    | Operation - Authenticate Data      |
    | 12    | Operation - Decrypt Encryption Key |
    | 13    | Operation - Decrypt Data           |

* Return Values and their meanings

    | Value | Meaning                            |
    |:-----:|------------------------------------|
    |   0F  | CSF Header                         |
    |   01  | SRK Table                          |
    |   02  | Key Certificate                    |
    |   03  | Signature                          |
    |   04  | Public Key                         |
    |   10  | Operation - Install Key            |
    |   11  | Operation - Authenticate Data      |
    |   12  | Operation - Decrypt Encryption Key |
    |   13  | Operation - Decrypt Data           |

# Quickstart Guide

## Prerequisites
  1. Python 3.7 or higher
  2. [RISC-V GNU Toolchain](https://github.com/riscv/riscv-gnu-toolchain)
  3. [Spike](https://github.com/riscv/riscv-isa-sim/)

## Usage
The first step involves generating an executable that needs to be signed. In this example, we will be using a simple hello world program. This executable can be generated using the following commands:
``` Bash
    $ cd lib/
    $ make hello
    $ cd ..
```

Next, install the signing tool:
``` Bash
    $ cd cst/
    $ pip install --editable .
    $ cst --help
```

To initialize all the files and keys required by the tool, run the following commands:
``` Bash
    $ mkdir work
    $ cd work
    $ cst init
    $ cd ..
```

NOTE: Enter a password when prompted while executing the cst command. The command would also subsequently ask for some user configurable parameters. For default values, proceed by leaving the fields blank, and pressing "Enter".

To sign the executable, execute the following command, and enter the passowrd that was set in the *cst init* command:
``` Bash
    $ cst -cf ./work/config.ini gen-csf --csf-file examples/hello_world.yaml
```

The above command would generate a hello.elf file that can be executed on Spike. Since Spike requires both, the BootROM and the generated hello.elf, we use GDB (along with OpenOCD) to load these programs. In order to generate the boot code, run:
```Bash
    $ cd ../lib/
    $ make fusefile=../cst/work/fuse.bin mkfile=../cst/work/mk.bin debug=2 lib rom
```

Finally, to simulate, open three terminals (with your current working directory as *lib*) and run 
1. Spike on Terminal 1 :
```Bash
    $ spike -m0x20000:0x3000,0x01000000:0x2000000,0x80000000:0x2000000 --rbb-port=9824 -H output/hello.elf
```

NOTE: The three memory regions that have been defined hold:

- The master key, secret key, public key store, and private key store (at 0x20000)
- The BootROM (at 0x01000000)
- The program to be loaded, which is signed, and possibly encrypted (at 0x80000000)


2. OpenOCD on Terminal 2:
```Bash
    $ openocd -f spike.cfg
```

3. GDB on Terminal 3:
```Bash
    $ riscv64-unknown-elf-gdb -x test.gdb
```

On successful execution, you should be able to see the following output on Terminal 1:
```
IN BOOTROM
(INFO) IN BOOTROM.
(INFO) Installing SRK.
(INFO) Install Key Success.
(INFO) Installing SignKey.
(INFO) Install Key Success.
(INFO) Authenticating CSF.
(INFO) Authenticate success
(INFO) Authenticating Image Data.
(INFO) Authenticate success
(INFO) Install Key Success.
(INFO) Decrypting Image Data.
(INFO) Decrypt success
(INFO) BOOT SUCESS, JUMPING TO FSBL
Hello, World!
```

In order to quit, do the following (in this order):
1. Terminal 3: Enter *q*
2. Terminal 2: Press *Ctrl + C*
3. Terminal 1: Press *Ctrl + C* and enter *q*


## CSF Input File

A sample CSF input file has been given below which can be used to load an encrypted and signed program. The CSF file has been annotated to indicate the functionality of each field.
The steps involved are also numerated below in comments.

``` yaml
    #Illustrative Command Sequence File Description
    - Type: Header
      # CSF version 
      CSF Version : 1.5
      # Version of the Program 
      Program Version: 0
      # Crypto algorithm used for hashing 
      Hash Algorithm : sha256
      
    #1. Install the SRK  
    - Type: Install SRK
      # Names of the keys
      Name : srk0 srk1 srk2 srk3
      # Path to the fuse file. Used to cross verify the srk table while generation.
      Fuse : ./work/fuse.bin
      # Index of the key location in the SRK table to be installed
      Source index : 0

    #2. Install a key (K1) that has been signed using the SRK. This key would be used to authenticate the CSF and the program.  
    - Type: Install Key
      # Key slot index used to authenticate the key to be installed
      Verification index : 0
      # Target key slot in HAB key store where key will be installed
      Target Index : 2
      # Key to install
      Name : signk1.1
    
    #3. Authenticate the CSF using K1
    - Type: Authenticate CSF
      # Index of the key in the PKI Store
      Verification index : 2

    #4. Install a secret key (S1) which will be used to decrypt the program that is to be loaded.
    - Type: Install Secret Key
      # Master Key slot index used to authenticate the key to be installed
      Verification index : 0
      # Target key slot in secret key store where key will be installed
      Target Index : 2
      # Key to install
      Name : dek
      
    #5. Verify the authenticity of the encrypted program using key K1      
    - Type: Authenticate Data
      # Key slot index used to authenticate the image data
      Verification index : 2
      # Modify the existing elf file to inject the csf 
      Modify : True
      # Set the Load flag on the section in the elf
      Load : True
      # Path to the output file
      Output : ../lib/output/hello.elf
      # Path to the binary file 
      File : ../lib/output/hello.riscv
      # Address for the loading the csf section onto 
      Address : 0x80005000
      # Chain the Blocks to produce a single signature
      Chain : True
      # The node with the description for the blocks
      # Each entry in the array is of the following format:
      # <start address> <offset> <length> 
      Blocks : 
       - 0x80000000 0x000 0x00010

    #6. Decrypt the program using S1  
    - Type: Decrypt Data
      # The index of the key in the Secret Key Table 
      Key Index : 2
      # Name of the key in the Database 
      Name : dek
      # The Crypto Algorithm for the encryption 
      Cipher : AES
      # The mode of Operation for the Crypto Algorithm 
      Mode : ECB
      # The node with the description for the blocks
      # Each entry in the array is of the following format:
      # <start address> <offset> <length>
      Blocks : 
        - 0x80000000 0x000 0x100
```

# Rollback Prevention
The bootrom is equipped with a rollback prevention mechanism which ensures that the FSBL cannot be downgraded to any previous versions. The csf for the FSBL contains a field `prog_version` in the csf header file. The value of this field is parsed and compared to a fuse value(`version_fuse`) on the device on boot. The condition `prog_version >= version_fuse` should be satisfied for a successful boot. If `prog_version == version_fuse`, the boot process proceeds as normal. If
`prog_version > version_fuse`, the value of the `version_fuse` is updated after successful authentication of the CSF and the image. This ensures that only valid and allowed updates to the FSBL cause a change to the fuse on the device. The update can be performed by any method. The bootrom does not enforce any specific update protocols. The simplest way to carry out an update is to replace the entire FSBL and csf with the respective newer versions.
