#ifndef __SECLIBROM__
#define __SECLIBROM__
typedef unsigned char uint8_t;
typedef int sec_id;
typedef struct sec_ctx{
    char pk_idx_1,pk_idx_2,sk_idx_1;
    uint8_t auth_status;
}sec_ctx;
sec_id alloc_sec_ctx();
int check_version(uint8_t ver);
int update_version(sec_id id,uint8_t ver);
void free_sec_ctx(sec_id id);
int install_key(sec_id id,uint8_t type_of_key,uint8_t crypto_algo,uint8_t source_index,uint8_t tgt_index,void *pointer_to_certificate);
int revoke_public_key(sec_id id,uint8_t index);
int revoke_secret_key(sec_id id,uint8_t index);
int decrypt_data(sec_id id,uint8_t crypto_algo, uint8_t mode, uint8_t index, void *blocks, int num, void *iv);
int auth_csf(sec_id id,uint8_t hash_algo, uint8_t crypto_algo, uint8_t pki_store_index,void *blocks, int num, void *pointer_to_signature);
int auth_data(sec_id id,uint8_t hash_algo, uint8_t crypto_algo, uint8_t pki_store_index,void *blocks, int num, void *pointer_to_signature);
void initialise_lib();
#endif
