#ifndef __SECLIBDEFINES__
#define __SECLIBDEFINES__

// CSF Tag values
#define TAG_csf_header                         0x0F
#define TAG_srk_table                          0x01
#define TAG_key_certificate                    0x02
#define TAG_signature                          0x03
#define TAG_public_key                         0x04
#define TAG_secret_key_entry                   0x05
#define OP_install_key            0x10
#define OP_authenticate_csf       0x11
#define OP_authenticate_data      0x12
#define OP_decrypt_data           0x13

// Key types
#define SRK 1
#define PK 2
#define SK 3

// Cipher Modes
#define MODE_CBC 1
#define MODE_CTR 2
#define MODE_ECB 3

// Error codes
#define ERR_SRC_IDX_OUT_OF_BOUNDS -0x10
#define ERR_TGT_IDX_OUT_OF_BOUNDS -0x11
#define ERR_OVR_VALID_ENTRY       -0x20
#define ERR_SRK_REVOKED           -0x21
#define ERR_SIG_MISMATCH          -0x30
#define ERR_INVALID_TAG           -0x40
#define ERR_UNRECOGNIZED_COMMAND  -0x41
#define ERR_INVALID_LEN           -0x42
#define ERR_INVALID_KEY           -0x50
#define ERR_INVALID_SRK_N         -0x51
#define ERR_INVALID_CIPHER_MODE   -0x60
#define ERR_INSUFF_PERMISSIONS    -0x70
#define SUCCESS 1
#define UPDATE_VERSION 2 

#ifdef RV64
typedef unsigned long int addr;
#define EXTRACT_ADDR EXTRACT_DWORD
#define TO_BLOCKS(var,addr,len,i)  \
    var[i+0] = (unsigned char)((unsigned long int)addr>>56);  \
    var[i+1] = (unsigned char)((unsigned long int)addr>>48);  \
    var[i+2] = (unsigned char)((unsigned long int)addr>>40);   \
    var[i+3] = (unsigned char)((unsigned long int)addr>>32);      \
    var[i+8] = (unsigned char)((unsigned long int)len>>56);   \
    var[i+9] = (unsigned char)((unsigned long int)len>>48);   \
    var[i+10] = (unsigned char)((unsigned long int)len>>40);    \
    var[i+11] = (unsigned char)((unsigned long int)len>>32);         \
    var[i+4] = (unsigned char)((unsigned long int)addr>>24);  \
    var[i+5] = (unsigned char)((unsigned long int)addr>>16);  \
    var[i+6] = (unsigned char)((unsigned long int)addr>>8);   \
    var[i+7] = (unsigned char)((unsigned long int)addr);      \
    var[i+12] = (unsigned char)((unsigned long int)len>>24);   \
    var[i+13] = (unsigned char)((unsigned long int)len>>16);   \
    var[i+14] = (unsigned char)((unsigned long int)len>>8);    \
    var[i+15] = (unsigned char)(unsigned long int)len; 
#else
typedef unsigned int addr;
#define EXTRACT_ADDR EXTRACT_WORD
#define TO_BLOCKS(var,addr,len,i)  \
    var[i+0] = (unsigned char)((int)addr>>24);  \
    var[i+1] = (unsigned char)((int)addr>>16);  \
    var[i+2] = (unsigned char)((int)addr>>8);   \
    var[i+3] = (unsigned char)((int)addr);      \
    var[i+4] = (unsigned char)((int)len>>24);   \
    var[i+5] = (unsigned char)((int)len>>16);   \
    var[i+6] = (unsigned char)((int)len>>8);    \
    var[i+7] = (unsigned char)(int)len; 
#endif


#define EXTRACT_DWORD(ptr) (unsigned long int)0+(((unsigned long)*(ptr))<<56)+(((unsigned long)*(ptr+1))<<48)+(((unsigned long)*(ptr+2))<<40)+(((unsigned long)*(ptr+3))<<32)+(((unsigned long)*(ptr+4))<<24)+(*(ptr+5)<<16)+(*(ptr+6)<<8)+*(ptr+7)+(unsigned long int)0
#define EXTRACT_WORD(ptr) (unsigned int) 0+(*(ptr)<<24)+(*(ptr+1)<<16)+(*(ptr+2)<<8)+*(ptr+3)
#define EXTRACT_HWORD(ptr) (unsigned int) 0+(*(ptr)<<8)+*(ptr+1)
#endif
