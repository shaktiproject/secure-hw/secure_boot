#include "debug.h"
#include "defines.h"
#include "seclib.h"
extern long int __secvt__;
unsigned long int *secvt = &(__secvt__);
int validate_program(void *csf_addr)
{
    sec_id id = -1;
    while(id==-1)
        id = ALLOC_CTX(secvt)();
    uint8_t *csf = csf_addr;
    lDebug(3,"%s address: %x",debugV(csf));
    uint8_t *temp = csf;
    if (*(temp++) != TAG_csf_header)
    {
        lDebug(1,"Incorrect CSF");
        return 0;
    }
    int csf_len = EXTRACT_HWORD(temp);
    temp+=4;
    int status = 0;
    while( csf + csf_len > temp)
    {

        lDebug(3,"%s: %x",debugV(temp));
        if (*temp == OP_install_key )
        {
            temp++;
            temp++;
#if DEBUG>0
            switch(*temp)
            {
                case 1:
                    lDebug(2,"Installing SRK.");
                    break;
                case 2:
                    lDebug(2,"Installing SignKey.");
                    break;
                case 3:
                    lDebug(3,"Installing Secret Key.");
                    break;
                default:
                    lDebug(3,"Unsupported Key type in Install Key.");
            }
#endif
            int offset = EXTRACT_WORD(temp+4);
            status = INSTALL_KEY(secvt)(id,*temp,*(temp+1),*(temp+2),*(temp+3),csf+offset);
            temp=temp+8;
#if DEBUG>0
            if (status == 1){
                lDebug(2,"Install Key Success.");
            }
            else
                lDebug(1,"Install Key Fail. Error Code: %x",status);
#endif
        }
        else if(*temp==OP_authenticate_csf)
        {
            int len = (unsigned char )*(temp+1);
            temp+=2;
            if (len == 9)
            {
                unsigned char blocks[2*sizeof(addr)];
                TO_BLOCKS(blocks,csf,csf_len,0);
                lDebug(2,"Authenticating CSF.");
                unsigned int offset = EXTRACT_WORD(temp+3);
                status = AUTHENTICATE_CSF(secvt)(id,*(temp),*(temp+1),*(temp+2),blocks,1,csf+offset);
                temp+=7;
            }
            else if (len == 17)
            {
                unsigned char blocks[4*sizeof(addr)];
                unsigned int sec_offset=EXTRACT_WORD(temp+7),sec_len=EXTRACT_WORD(temp+11);
                TO_BLOCKS(blocks,csf,csf_len,0);
                TO_BLOCKS(blocks,csf+sec_offset,sec_len,2*sizeof(addr));
                lDebug(2,"Authenticating CSF.");
                unsigned int offset = EXTRACT_WORD(temp+3);
                status = AUTHENTICATE_CSF(secvt)(id,*(temp),*(temp+1),*(temp+2),blocks,2,csf+offset);
                temp+=15;
            }
#if DEBUG>0
            if(status == 1)
                lDebug(2,"Authenticate success");
            else
                lDebug(1,"Authenticate Fail. Error Code: %x",status);
#endif
        }
        else if(*temp==OP_authenticate_data)
        {
            int len = (unsigned char )*(temp+1);
            temp+=2;
            if (((len-9)% (2*sizeof(addr))) == 0)
            {
                int num = (len-9)/(2*sizeof(addr));
                int sign_offset = EXTRACT_WORD(temp+3);
                lDebug(2,"Authenticating Image Data.");
                status = AUTHENTICATE_DATA(secvt)(id,*(temp),*(temp+1),*(temp+2),(temp+7),num,csf+sign_offset);
                temp+=len-2;
            }
#if DEBUG>0
            if(status == 1)
                lDebug(2,"Authenticate success");
            else
                lDebug(1,"Authenticate Fail. Error Code: %x",status);
#endif
        }
        else if(*temp==OP_decrypt_data)
        {
            int len = (unsigned char )*(temp+1);
            temp+=2;
            int num = (len-9)/(2*sizeof(addr));
            int iv_offset = EXTRACT_WORD(temp+3);
            lDebug(2,"Decrypting Image Data.");
            status = DECRYPT_DATA(secvt)(id,*(temp),*(temp+1),*(temp+2),(temp+7),num,csf+iv_offset);
            temp+=len-2;
#if DEBUG>0
            if(status==1)
                lDebug(2,"Decrypt success");
            else
                lDebug(1,"Decrypt Fail");
#endif
        }
        else
        {
            lDebug(1,"Unrecognised csf command. Error Code: %x",status);
            status=0;
        }
        if (status != 1)
        {
            lDebug(1,"CSF Verification Fail. Error Code: %x",status);
            break; 
        }
    }
    FREE_CTX(secvt)(id);
    return status;
}
