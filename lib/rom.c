#include<string.h>
#include "lib.h"
#include "debug.h"
#include "csfparser.h"
#include "seclib.h"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"
#pragma GCC diagnostic ignored "-Wimplicit-int"
#pragma GCC diagnostic ignored "-Wreturn-type"
#pragma GCC diagnostic ignored "-Wpointer-to-int-cast"
void jump_to(void* jump_addr)
{
   volatile register unsigned long* a0 asm("a0") = (unsigned long*) jump_addr;
   asm volatile("jr a0");
}
typedef void (*func)(void);
func sec_vector_table[] __attribute__(( section( ".rodata.secvt" ) )) = {alloc_sec_ctx,install_key,auth_data,auth_csf,decrypt_data,revoke_public_key,revoke_secret_key,free_sec_ctx};
int main(int argc, char *argv[])
{  
#ifdef CST_LIB_VERSION
    printf(" \n CST LIBRARY GIT VERSION %s\n",CST_LIB_VERSION);
#endif
    printf("IN BOOTROM\n");
    lDebug(2,"IN BOOTROM.");
    int boot_image=0;
    initialise_lib();
    boot_image = validate_program(*(unsigned int *)CSF_ADDRESS);
    if (boot_image == 1){
        lDebug(2,"BOOT SUCESS, JUMPING TO FSBL");
#if !defined(OPENSBI)
    	__asm__("li x1,0x80000000; li x11,0x1020;jr x1;");
#endif

#ifdef OPENSBI
	reset_regs(); //Reset all registers after authenication
	__asm__("li a1,0x82700000;"); //Passing FDT address as a1 argument
	 __asm__("li a0,0x80500000; jr a0;"); //Passing start address of opensbi
#endif
    }
    else
    {
        lDebug(1,"BOOT FAILURE.");
    }
}
