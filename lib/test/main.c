#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sha256.h"
#include "defines.h"
#include "debug.h"
#define xstr(s) str(s)
#define str(s) #s
typedef unsigned char uint8_t;
static void digest_message(const uint8_t *message, int message_len, uint8_t *digest)
{
    SHA256_CTX ctx;
	sha256_init(&ctx);
	sha256_update(&ctx, message, message_len);
	sha256_final(&ctx, digest);
}
int verify_srk(uint8_t *table, uint8_t *fuse)
{
    lDebug(3,"Verifying SRK Table");
    lDebug(3,"%s: %x",debugV(table));
    if (*table == TAG_srk_table) {
        uint8_t digest[32],intermediate_hash[32*4],*temp = table+4;
        unsigned int len = 0,mlen=0;
        int i=0,srk_n = *(table+3),tlen = EXTRACT_HWORD(table+1);
        lDebug(3,"%s: %x",debugV(srk_n));
        uint8_t *cond = table+tlen;
        while(i<srk_n && temp<=cond)
        {
            mlen = EXTRACT_HWORD((temp+1));
            digest_message(temp,mlen,digest);
            memcpy(intermediate_hash+len,digest,32);
            len+=32;
            temp=temp+mlen;
            i+=1;
        }
        digest_message(intermediate_hash,len,digest);
        lDebug(3,"%s address: %x",debugV(fuse));
        debugX(3,fuse,32);
        debugX(3,digest,32);
        if (memcmp(digest,fuse,32)==0){
            lDebug(3,"SRK Hash Match Successful.");
            return SUCCESS;
        }
        else
        {
            lDebug(1,"SRK Table Hash Doesnt Match with fuse.");
            return ERR_SIG_MISMATCH;
        }
    }
    else
    {
        return ERR_INVALID_TAG;
    }
}
unsigned char* read_file(char* name,int len)
{
    unsigned char *data;
    data = (unsigned char *)malloc(len*sizeof(unsigned char));
    FILE *fp = fopen(name, "rb");
    if (!fp) {
	    fprintf(stderr, "unable to open: %s\n", name);
	    return NULL;
    }
    int n = fread(data,len,1,fp);
    fclose(fp);
    return data;
}
void main()
{
    uint8_t *fuse = read_file(xstr(FUSEFILE),32);
    uint8_t *table = read_file(xstr(TABFILE),2116);
    int code = verify_srk(table,fuse);
    printf("%x\n",-1*code);
    free(fuse);
    free(table);
}
