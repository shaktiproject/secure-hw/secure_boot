#include <string.h>
#include "lib.h"
#include "defines.h"
#include "debug.h" 
#include "sha256.h"
#include "./crypto/RSA/types.h"
#include "./crypto/AES/aes.h"
extern int __fuse__;
extern int __mk__;
extern int __control_fuses__;
extern int __version_fuse__;

typedef unsigned char uint8_t;
typedef struct public_key{
    bignum_t d,n,rr;
    uint8_t key_type;
}public_key;
typedef struct secret_key{
    uint8_t valid;
    uint8_t key[32];
}secret_key;


#define xstr(s) str(s)
#define str(s) #s
#ifndef NUM_PK_ENTRY
#define NUM_PK_ENTRY 10
#endif
#ifndef NUM_SRK_REVOKE_ENTRY
#define NUM_SRK_REVOKE_ENTRY 3
#endif
#ifndef NUM_SK_ENTRY
#define NUM_SK_ENTRY 10
#endif
#ifndef NUM_MK_ENTRY
#define NUM_MK_ENTRY 1
#endif
#ifndef NUM_CTX
#define NUM_CTX 1
#endif

#ifdef SPIKE
    #ifndef FUSEFILE
        #define FUSEFILE \"fuse.bin\"
    #endif
    __asm__(" .align 8\n.section .rodata.fuse\n");
    __asm__(xstr(.incbin FUSEFILE));
    #ifdef MKFILE
    __asm__(" .align 8\n.section .rodata.mk\n");
    __asm__(xstr(.incbin MKFILE));
    #endif
__asm__(".align 8\n.section .rodata.control_fuses\n.fill 1,1,0x00;");
__asm__(".align 8\n.section .rodata.version_fuse\n.fill 1,1,0x00;");
public_key pk_store[NUM_PK_ENTRY] __attribute__((section (".data.pkStore")));
secret_key sk_store[NUM_SK_ENTRY] __attribute__((section (".data.skStore")));
uint8_t srk_installed __attribute__((section(".data.ctx")))= 0xFF;
sec_ctx * ctx_ptr_store[NUM_CTX] __attribute__((section(".data.ctx")));
sec_ctx ctx_store[NUM_CTX] __attribute((section(".data.ctx")));
#endif

sec_id alloc_sec_ctx()
{
    sec_id id = -1;
    int i=0;
    for(i=0;i<NUM_CTX;++i)
        if (ctx_ptr_store[i] == NULL)
        {
            ctx_ptr_store[i] = &ctx_store[i];
            ctx_store[i].pk_idx_1 = -1;
            ctx_store[i].pk_idx_2 = -1;
            ctx_store[i].sk_idx_1 = -1;
            ctx_store[i].auth_status = 0;
            id = i;
            break;
        }
    return id;
}
int check_version(uint8_t ver)
{
    uint8_t fuse_val = *(&(__version_fuse__));
    if(fuse_val >= ver)
        return SUCCESS;
    else
        return ERR_INSUFF_PERMISSIONS;
}
int update_version(sec_id id,uint8_t ver)
{
    uint8_t fuse_val = *(&(__version_fuse__)+1);
    if ((fuse_val &(1 << srk_installed)) > 0)
        return ERR_INSUFF_PERMISSIONS;
    else{
        /*write fuse operation*/
        *(&(__version_fuse__)) = ver;
        return SUCCESS;
    }
}
void free_sec_ctx(sec_id id)
{
    if(id<NUM_CTX)
        ctx_ptr_store[id] = NULL;
}

static void digest_message(const uint8_t *message, int message_len, uint8_t *digest)
{
    SHA256_CTX ctx;
	sha256_init(&ctx);
	sha256_update(&ctx, message, message_len);
	sha256_final(&ctx, digest);
}

static void digest_blocks(uint8_t *digest, uint8_t *blocks, int num)
{
    int i;
    SHA256_CTX ctx;
	sha256_init(&ctx);
	for(i=0;i<num;++i)
	{
        
        unsigned char *data = (unsigned char *)(EXTRACT_DWORD(blocks));
        addr len = EXTRACT_ADDR(blocks+sizeof(addr));
	    sha256_update(&ctx, data, len);
        blocks = blocks+2*sizeof(addr);	
    }
	sha256_final(&ctx, digest);
}

static int install_secret_key(uint8_t *blob, int mk_index, int tgt_index)
{
    lDebug(3,"Installing Secret Key");
    if (sk_store[tgt_index].valid != 0)
    {
        lDebug(1,"Attempting overwrite of valid secret key entry");
        return ERR_OVR_VALID_ENTRY;
    }
    if(*blob == TAG_secret_key_entry)
    {
        blob=blob+3;
        lDebug(3,"Verifying Ciphertext Integrity");
        lDebug(3,"%s: %x",debugV(blob));
        uint8_t *master_key = &(__mk__);
        struct AES_ctx ctx;
        uint8_t *iv = blob+43;
        AES_init_ctx_iv(&ctx, master_key+32*mk_index, iv);
        uint8_t calculated[32], buf[32];
        memcpy(buf,iv+16,32);
        debugX(3,iv,16);
        debugX(3,buf,32);
        AES_CBC_decrypt_buffer(&ctx,buf,32);
        debugX(3,buf,32);
        digest_message(blob+11,32,calculated);
        debugX(3,calculated,32);
        if (memcmp(buf,calculated,32)==0){
            lDebug(3,"Ciphertext Integrity Verification Successful. ");
            memcpy(buf,iv-32,32);
            debugX(3,buf,32);
            lDebug(3,"Decrypting and Installing Secret Key. ");
            AES_init_ctx_iv(&ctx, master_key, iv);
            AES_CBC_decrypt_buffer(&ctx,buf,32);
            debugX(3,buf,32);
            memcpy(&(sk_store[tgt_index].key),buf,32);
            sk_store[tgt_index].valid = 1;
            return SUCCESS;
        }
        else
        {
            lDebug(1,"Ciphertext hash doesn't match calculated hash.");
            return ERR_SIG_MISMATCH; 
        }
    }
    else
    {
        return ERR_INVALID_TAG;
    }
}
static int verify_srk(uint8_t *table)
{
    lDebug(3,"Verifying SRK Table");
    lDebug(3,"%s: %x",debugV(table));
    if (*table == TAG_srk_table) {
        uint8_t *fuse,digest[32],intermediate_hash[32*4],*temp = table+4;
        fuse = &(__fuse__);
        unsigned int len = 0,mlen=0;
        int i=0,srk_n = *(table+3),tlen = EXTRACT_HWORD(table+1);
        lDebug(3,"%s: %x",debugV(srk_n));
        uint8_t *cond = table+tlen;
        while(i<srk_n && temp<=cond)
        {
            mlen = EXTRACT_HWORD((temp+1));
            digest_message(temp,mlen,digest);
            memcpy(intermediate_hash+len,digest,32);
            len+=32;
            temp=temp+mlen;
            i+=1;
        }
        digest_message(intermediate_hash,len,digest);
        lDebug(3,"%s address: %x",debugV(fuse));
        debugX(3,fuse,32);
        debugX(3,digest,32);
        if (memcmp(digest,fuse,32)==0){
            lDebug(3,"SRK Hash Match Successful.");
            return SUCCESS;
        }
        else
        {
            lDebug(1,"SRK Table Hash Doesnt Match with fuse.");
            return ERR_SIG_MISMATCH;
        }
    }
    else
    {
        return ERR_INVALID_TAG;
    }
}
static int load_key(bignum_t *d,bignum_t *n, bignum_t *rr, uint8_t *cert)
{
    uint8_t *temp = cert;
    int cert_len = EXTRACT_HWORD((temp+1));
    lDebug(3,"Installing Key. %s: %d",debugV(cert));
    if (*(temp) == TAG_public_key){
        temp = temp+3+4;
        int mod_len,exp_len,rr_len;
        mod_len = EXTRACT_HWORD(temp);
        exp_len = EXTRACT_HWORD((temp+2));
        rr_len = EXTRACT_HWORD((temp+4));
        temp = temp+6;
        BN_init(d);
        BN_init(n);
        BN_init(rr);
        BN_bin2bn(n,temp,0,mod_len);
        BN_bin2bn(d,temp+mod_len,0,exp_len);
        BN_bin2bn(rr,temp+mod_len+exp_len,0,rr_len);
        #if DEBUG>0
            char mod[1000],exp[1000],RR[1000];
            BN_bn2hex(d,mod);
            BN_bn2hex(n,exp);
            BN_bn2hex(rr,RR);
            lDebug(3,"%s: %s",debugV(mod));
            lDebug(3,"%s: %s",debugV(exp));    
            lDebug(3,"%s: %s",debugV(RR));
            lDebug(3,"Key Install Successful.");
        #endif
        return SUCCESS;
    }
    else
    {
        return ERR_INVALID_TAG;
    }
}
static int RSAsignVerify(uint8_t *hash,int hash_len,uint8_t *sign,int sig_len,int verif_index)
{
    lDebug(3,"Verifying Signature");
    if ((*sign == TAG_signature) && (EXTRACT_HWORD(sign+1)==sig_len)){
        sign = sign+3;
        uint8_t msg[256];
        bignum_t sig,val;
        BN_init(&sig);
        BN_init(&val);
        BN_bin2bn(&sig,sign,0,sig_len);
        rsaDecrypt(&val,&sig,&(pk_store[verif_index].d),&(pk_store[verif_index].n), (pk_store[verif_index].rr));
        int len = BN_bn2bin(&val,msg);
        #if DEBUG>0
            char SIG[1000],MSG[1000];
            BN_bn2hex(&sig,SIG);
            BN_bn2hex(&val,MSG);
            lDebug(3,"%s: %s",debugV(SIG));
            lDebug(3,"%s: %s",debugV(MSG));
        #endif 
        if (memcmp(msg+len-hash_len,hash,hash_len)==0){
            lDebug(3,"Signature Match.");
            return SUCCESS;
        }
        else
        {
            return ERR_SIG_MISMATCH;
        }
    }
    else
    {
        lDebug(1,"Signature Verification Failure.");
        return ERR_INVALID_TAG;
    }
}
static int verify_certificate(uint8_t *cert,int verif_index)
{
    if(*((uint8_t *)cert) == TAG_key_certificate)
    {
        lDebug(3,"Verifying Certificate.");
        uint8_t *temp = cert+3,digest[32];
        unsigned int len = EXTRACT_HWORD(temp-2);
        int pk_len = EXTRACT_HWORD((temp+1)),i=0;
        if (*(temp) == TAG_public_key)
        {
            temp = temp+3+4;
            int mod_len,exp_len,rr_len,sig_len;
            mod_len = EXTRACT_HWORD(temp);
            exp_len = EXTRACT_HWORD((temp+2));
            rr_len = EXTRACT_HWORD((temp+4));
            temp = temp+6+mod_len+exp_len+rr_len;
            if (*(temp) == 0x03){
                sig_len = EXTRACT_HWORD(temp+1);
                digest_message(cert+3,pk_len+3,digest);
                return(RSAsignVerify(digest,32,temp,sig_len,verif_index));
            }
        }
        else 
        {
            return ERR_INVALID_TAG;
        }
    }
    else
    {
        return ERR_INVALID_TAG;
    }
}
int install_key(sec_id id,uint8_t type_of_key,uint8_t crypto_algo,uint8_t source_index,uint8_t tgt_index,void *pointer_to_certificate)
{
    int result=0;
    if (type_of_key == PK)
    {
        lDebug(3,"%s: %d %s: %d %d ",debugV(source_index),debugV(tgt_index));
        if (source_index>=NUM_PK_ENTRY)
        {
            lDebug(1,"Source Index out of bounds.");
            return ERR_SRC_IDX_OUT_OF_BOUNDS;
        }
        if (tgt_index>=NUM_PK_ENTRY)
        {
            lDebug(1,"Target Index out of bounds.");
            return ERR_TGT_IDX_OUT_OF_BOUNDS;
        } 
        if (pk_store[source_index].key_type != 1)
        {
            lDebug(1,"Invalid source key type.");
            return ERR_INVALID_KEY;
        }
        if (pk_store[tgt_index].key_type != 0)
        {
            lDebug(1,"Attempting overwrite of valid target entry.");
            return ERR_OVR_VALID_ENTRY;
        }
        if ((ctx_ptr_store[id])->pk_idx_1 != 255 || (ctx_ptr_store[id])->pk_idx_2 != 255)
        {
            lDebug(1,"Exceeding Max Number of Key Installs.");
            return ERR_INSUFF_PERMISSIONS;

        }
        uint8_t *cert=pointer_to_certificate;
        result = verify_certificate(cert,source_index);
        switch(result)
        {     
            case SUCCESS:
                result = load_key(&(pk_store[tgt_index].d),&(pk_store[tgt_index].n),&(pk_store[tgt_index].rr),cert+3);
                pk_store[tgt_index].key_type = type_of_key;
                if ((ctx_ptr_store[id])->pk_idx_1 == 255)
                {
                    (ctx_ptr_store[id])->pk_idx_1 = tgt_index;
                }
                else if ((ctx_ptr_store[id])->pk_idx_2 == 255)
                {
                    (ctx_ptr_store[id])->pk_idx_2 = tgt_index;
                }
                break;
            default:
                return result; 
        }
    }
    else if(type_of_key==SRK)
    {
        if (srk_installed != 0xFF)
        {
            lDebug(1,"Attempt to reinstall SRK.");
            return ERR_INSUFF_PERMISSIONS;
        }
        uint8_t fuse_val = *(&(__control_fuses__));
        lDebug(3,"%s: %02x",debugV(fuse_val));
        uint8_t *cert=pointer_to_certificate;
        unsigned int srk_n=*(cert+3);
        lDebug(3,"%s: %02x",debugV(srk_n));
        if (srk_n>NUM_SRK_REVOKE_ENTRY+1)
        {
            lDebug(1,"Invalid number of SRKs in SRK Table");
            return ERR_INVALID_SRK_N;
        }
        if (source_index>=srk_n)
        {
            lDebug(1,"Source Index out of bounds.");
            return ERR_SRC_IDX_OUT_OF_BOUNDS;
        }
        if((fuse_val&(1<<source_index)) != 0){
            lDebug(1,"SRK index revoked.");
            return ERR_SRK_REVOKED;
        }
        if (tgt_index>=NUM_PK_ENTRY)
        {
            lDebug(1,"Target Index out of bounds.");
            return ERR_TGT_IDX_OUT_OF_BOUNDS;
        } 
        if (pk_store[tgt_index].key_type != 0)
        {
            lDebug(1,"Attempting overwrite of valid target entry.");
            return ERR_OVR_VALID_ENTRY;
        }
        result = verify_srk(cert);
        unsigned int mlen,i = 0,srk_len = EXTRACT_HWORD(cert+1);
        switch(result)
        {
            case SUCCESS:
                cert=cert+4;
                while(i<srk_n) 
                {
                    mlen = EXTRACT_HWORD((cert+1));
                    if(i==source_index) 
                    {            
                        result = load_key(&(pk_store[tgt_index].d),&(pk_store[tgt_index].n),&(pk_store[tgt_index].rr),cert);
                        break;
                    }
                    cert+=mlen;
                    i++;
                }
                pk_store[tgt_index].key_type = type_of_key;
                srk_installed = source_index;
                break;
            default:
                return result;
        } 
    }
    else if(type_of_key==SK)
    {
        if (source_index>=NUM_MK_ENTRY)
        {
            lDebug(1,"Source Index out of bounds.");
            return ERR_SRC_IDX_OUT_OF_BOUNDS;
        }
        if (tgt_index>=NUM_SK_ENTRY)
        {
            lDebug(1,"Target Index out of bounds.");
            return ERR_TGT_IDX_OUT_OF_BOUNDS;
        }
        if ((ctx_ptr_store[id])->sk_idx_1 != 255 || (ctx_ptr_store[id])->auth_status != 1)
        {
            lDebug(1,"Exceeding Max Number of Secret Key Installs.");
            return ERR_INSUFF_PERMISSIONS;

        }
        uint8_t *blob = pointer_to_certificate;
        result = install_secret_key(pointer_to_certificate, source_index, tgt_index);
        switch(result)
        {
            case SUCCESS:
                (ctx_ptr_store[id])->sk_idx_1 = tgt_index;
                break;
        }
    }
    return result;
}

int revoke_public_key(sec_id id,uint8_t index)
{
    if (index < NUM_PK_ENTRY)
    {
        if ((ctx_ptr_store[id])->pk_idx_1 == index) 
        {
            pk_store[index].key_type = 0x0;
            (ctx_ptr_store[id])->pk_idx_1 = 255;
        }
        else if ((ctx_ptr_store[id])->pk_idx_2 == index)
        {
            pk_store[index].key_type = 0x0;
            (ctx_ptr_store[id])->pk_idx_2 = 255;
        }
        else
        {
            lDebug(1,"Attempting to delete key at index %d not allowed.",index);
            return ERR_INSUFF_PERMISSIONS;
        }
    }
    else
    {
        return ERR_SRC_IDX_OUT_OF_BOUNDS;
    }
    return SUCCESS;
}

int revoke_secret_key(sec_id id,uint8_t index)
{
    if (index < NUM_SK_ENTRY)
    {
        if ((ctx_ptr_store[id])->sk_idx_1 == index)
        {
            sk_store[index].valid = 0x0;
            (ctx_ptr_store[id])->sk_idx_1 = 255;
        }
        else
        {
            lDebug(1,"Attempting to delete key at index %d not allowed.",index);
            return ERR_INSUFF_PERMISSIONS;
        }
    }
    else
    {
        return ERR_SRC_IDX_OUT_OF_BOUNDS;
    }
    return SUCCESS;
}

int decrypt_data(sec_id id,uint8_t crypto_algo, uint8_t mode, uint8_t index, void *blocks, int num, void *iv)
{
    lDebug(3,"Decrypt Data");
    lDebug(3,"%s: %x",debugV(iv));
    struct AES_ctx ctx;
    if (sk_store[index].valid != 1)
    {
        return ERR_INVALID_KEY;
    }
    if (index >= NUM_SK_ENTRY){
        return ERR_SRC_IDX_OUT_OF_BOUNDS;
    }
    if ((ctx_ptr_store[id])->sk_idx_1 != index)
    {
        return ERR_INSUFF_PERMISSIONS;
    }
    uint8_t *iv_ptr = iv;
    AES_init_ctx_iv(&ctx, &(sk_store[index].key), iv_ptr);
    lDebug(3,"%s : %x", debugV(index));
    lDebug(3,"%s : %x", debugV(num));
    lDebug(3,"%s : %x", debugV(mode));
    debugX(3,iv_ptr,16);
    int i;
    uint8_t *blocks_ptr = blocks;
    void (*interface) (struct AES_ctx *,uint8_t *, int) ;
    switch(mode){
        case MODE_CBC:
            interface= &AES_CBC_decrypt_buffer;
            break;
        case MODE_CTR:
            interface= &AES_CTR_xcrypt_buffer;
            break;
        case MODE_ECB:
           interface= &AES_ECB_decrypt_buffer;
           break;
        default:
           return ERR_INVALID_CIPHER_MODE;
    }
    for(i=0;i<num;++i)
	{
        unsigned char *data = (unsigned char *) EXTRACT_ADDR(blocks_ptr);
        addr len = EXTRACT_ADDR(blocks_ptr+sizeof(addr));
	    interface(&ctx, data, len);
        blocks_ptr = blocks_ptr+2*sizeof(addr);	
    }
    return SUCCESS;
}
int auth_csf(sec_id id,uint8_t hash_algo, uint8_t crypto_algo, uint8_t pki_store_index,void *blocks, int num, void *pointer_to_signature)
{
    lDebug(3,"Authenticate Data.");
    int result=0;
    uint8_t *sign = pointer_to_signature;
    lDebug(3,"%s addr: %x",debugV(sign));
    lDebug(3,"%s : %x", debugV(pki_store_index));
    lDebug(3,"%s : %x", debugV(num));
    if(pki_store_index >= NUM_PK_ENTRY)
    {
        return ERR_SRC_IDX_OUT_OF_BOUNDS;
    }
    if(pk_store[pki_store_index].key_type != PK)
    {
        return ERR_INVALID_KEY;
    }
    if (*sign != TAG_signature)
    {
        return ERR_INVALID_TAG;
    }
    if(pki_store_index >= NUM_PK_ENTRY)
    {
        return ERR_SRC_IDX_OUT_OF_BOUNDS;
    }
    uint8_t hash[32];
    digest_blocks(hash,blocks,num);
    result = RSAsignVerify(hash,32,sign,EXTRACT_HWORD(sign+1),pki_store_index);
    if (result == SUCCESS)
    {
        (ctx_ptr_store[id])->auth_status = 1;
    }
    return result;
}
int auth_data(sec_id id,uint8_t hash_algo, uint8_t crypto_algo, uint8_t pki_store_index,void *blocks, int num, void *pointer_to_signature)
{
    if ((ctx_ptr_store[id])->auth_status != 1)
    {
        lDebug(1,"Authenticate CSF first");
        return ERR_INSUFF_PERMISSIONS;
    }
    lDebug(3,"Authenticate Data.");
    int result=0;
    uint8_t *sign = pointer_to_signature;
    lDebug(3,"%s addr: %x",debugV(sign));
    lDebug(3,"%s : %x", debugV(pki_store_index));
    lDebug(3,"%s : %x", debugV(num));
    if(pki_store_index >= NUM_PK_ENTRY)
    {
        return ERR_SRC_IDX_OUT_OF_BOUNDS;
    }
    if(pk_store[pki_store_index].key_type != PK)
    {
        return ERR_INVALID_KEY;
    }
    if (*sign != TAG_signature)
    {
        return ERR_INVALID_TAG;
    }
    if(pki_store_index >= NUM_PK_ENTRY)
    {
        return ERR_SRC_IDX_OUT_OF_BOUNDS;
    }
    uint8_t hash[32];
    digest_blocks(hash,blocks,num);
    result = RSAsignVerify(hash,32,sign,EXTRACT_HWORD(sign+1),pki_store_index);
    return result;
}
void initialise_lib()
{
    int i;
    for(i=0;i<10;++i)
    {
        pk_store[i].key_type=0;
        sk_store[i].valid = 0;
    }
}
