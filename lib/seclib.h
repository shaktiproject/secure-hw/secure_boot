#ifndef __SECLIB__
#define __SECLIB__
typedef unsigned char uint8_t;
typedef int sec_id;
typedef int (*install_key_func_t) (sec_id,uint8_t,uint8_t,uint8_t,uint8_t,void *);
typedef int (*revoke_public_key_func_t)(sec_id,uint8_t);
typedef int (*alloc_sec_ctx_func_t)();
typedef int (*free_sec_ctx_func_t)(sec_id);
typedef int (*revoke_secret_key_func_t)(sec_id,uint8_t);
typedef int (*decrypt_data_func_t)(sec_id,uint8_t, uint8_t, uint8_t, void *, int, void *);
typedef int (*auth_data_func_t)(sec_id,uint8_t, uint8_t, uint8_t,void *, int, void *);
typedef int (*auth_csf_func_t)(sec_id,uint8_t, uint8_t, uint8_t,void *, int, void *);
#define ALLOC_CTX(var) (*((alloc_sec_ctx_func_t)var[0]))
#define INSTALL_KEY(var) (*((install_key_func_t)var[1]))
#define AUTHENTICATE_DATA(var) (*((auth_data_func_t)var[2]))
#define AUTHENTICATE_CSF(var) (*((auth_csf_func_t)var[3]))
#define DECRYPT_DATA(var) (*((decrypt_data_func_t)var[4]))
#define REVOKE_PK(var) (*((revoke_public_key_func_t)var[5]))
#define REVOKE_SK(var) (*((revoke_secret_key_func_t)var[6]))
#define FREE_CTX(var) (*((free_sec_ctx_func_t)var[7]))
#endif
