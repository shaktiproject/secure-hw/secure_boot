# Secure Boot 
The secure boot framework guarantees that only signed binaries can be loaded and executed on particular hardware. The framework consists of a signing tool that runs on a host system, and a BootROM code that would be put on the target system. The signing tool generates signed binaries, and the BootROM code verifies the signature when the program is being loaded on the target. The framework also supports encrypted boot, where the signing tool generates encrypted and signed binaries, and, the bootrom would verify, decrypt and load that executable. More details of this framework can be found in docs/precis.md.

# License
All of the source code available in this repository is under the BSD license. Please refer to LICENSE.iitm for more details.

# Quickstart Guide
In order to generate a signed executable, and run it on Spike (riscv-isa-sim), follow the steps mentioned below.

## Prerequisites
  1. Python 3.7 or higher
  2. [RISC-V GNU Toolchain](https://github.com/riscv/riscv-gnu-toolchain)
  3. [Spike](https://github.com/riscv/riscv-isa-sim/)


## Usage
The first step involves generating an executable that needs to be signed. In this example, we will be using a simple hello world program. This executable can be generated using the following commands:
``` Bash
    $ cd lib/
    $ make hello
    $ cd ..
```

Next, install the signing tool:
``` Bash
    $ cd cst/
    $ pip install --editable .
    $ cst --help
```

To initialize all the files and keys required by the tool, run the following commands:
``` Bash
    $ mkdir work
    $ cd work
    $ cst init
    $ cd ..
```

NOTE: Enter a password when prompted while executing the cst command. The command would also subsequently ask for some user configurable parameters. For default values, proceed by leaving the fields blank, and pressing "Enter".

To sign the executable, execute the following command, and enter the passowrd that was set in the *cst init* command:
``` Bash
    $ cst -cf ./work/config.ini gen-csf --csf-file examples/hello_world.yaml
```

The above command would generate a hello.elf file that can be executed on Spike. Since Spike requires both, the BootROM and the generated hello.elf, we use GDB (along with OpenOCD) to load these programs. In order to generate the boot code, run:
```Bash
    $ cd ../lib/
    $ make fusefile=../cst/work/fuse.bin mkfile=../cst/work/mk.bin debug=2 lib rom
```

Finally, to simulate, open three terminals (with your current working directory as *lib*) and run 
1. Spike on Terminal 1 :
```Bash
    $ spike -m0x20000:0x3000,0x01000000:0x2000000,0x80000000:0x2000000 --rbb-port=9824 -H output/hello.elf
```

NOTE: The three memory regions that have been defined hold:

- The master key, secret key, public key store, and private key store (at 0x20000)
- The BootROM (at 0x01000000)
- The program to be loaded, which is signed, and possibly encrypted (at 0x80000000)


2. OpenOCD on Terminal 2:
```Bash
    $ openocd -f spike.cfg
```

3. GDB on Terminal 3:
```Bash
    $ riscv64-unknown-elf-gdb -x test.gdb
```

On successful execution, you should be able to see the following output on Terminal 1:
```
IN BOOTROM
(INFO) IN BOOTROM.
(INFO) Installing SRK.
(INFO) Install Key Success.
(INFO) Installing SignKey.
(INFO) Install Key Success.
(INFO) Authenticating CSF.
(INFO) Authenticate success
(INFO) Authenticating Image Data.
(INFO) Authenticate success
(INFO) Install Key Success.
(INFO) Decrypting Image Data.
(INFO) Decrypt success
(INFO) BOOT SUCESS, JUMPING TO FSBL
Hello, World!
```

In order to quit, do the following (in this order):
1. Terminal 3: Enter *q*
2. Terminal 2: Press *Ctrl + C*
3. Terminal 1: Press *Ctrl + C* and enter *q*
