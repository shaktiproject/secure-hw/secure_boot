import os
import codecs
import pip
from setuptools import setup, find_packages

import cst

# Base directory of package
here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()
def read_requires():
    with open(os.path.join(here, "cst/requirements.txt"),"r") as reqfile:
        return reqfile.read().splitlines()


#Long Description
with open("README.rst", "r") as fh:
    long_description = fh.read()

setup(name="cst",
      version=cst.__version__,
      description="Code Signing Tool",
      long_description=long_description,
      classifiers=[
          "Programming Language :: Python :: 3.7",
          "License :: OSI Approved :: BSD License",
          "Development Status :: 4 - Beta"
      ],
      license='BSD-3-Clause',
      packages=find_packages(),
      package_dir={'cst': 'cst/'},
      package_data={
          'cst': [
              'requirements.txt',
          ]
      },
      install_requires=read_requires(),
      python_requires=">=3.6.0",
      entry_points={
          "console_scripts": ["cst=cst.main:run"],
      })
