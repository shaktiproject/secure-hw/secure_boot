import configparser
import os,base64
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa,utils,padding
from datetime import datetime, timedelta
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
import cst.utils as cst_utils

bstr = lambda x, y: x.to_bytes(y,byteorder='big')

class State:
    def __init__(self,config,password):
        if(os.stat(config['db']).st_size==0):
            self.db = {}
        else:
            self.db = cst_utils.load_yaml(config['db'])
        self.db_file = config['db']
        self.password = password.encode('utf-8')
        self.key_dir = config['key_dir']
        self.crt_dir = config['crt_dir']

    def gen_srk_table(self,names):
        table = b''+bstr(len(names),1)
        intermediate_hash = b''
        for name in names:
            entry = self.__pub_key_entry(name)
            table+=entry
            intermediate_hash+=cst_utils.hash(entry)
        table = bstr(1,1)+bstr(len(table)+3,2)+table
        final_hash=cst_utils.hash(intermediate_hash)
        return table,final_hash

    def __pub_key_entry(self,name):
        entry = b''
        cert = self.__load_public_cert(name)
        public_key = cert.public_key()
        if isinstance(public_key,rsa.RSAPublicKey):
            entry+=bstr(0x21,1)+bstr(0,2)
        else:
            raise SystemExit
        if (cert.extensions.get_extension_for_class(x509.BasicConstraints).value).ca:
            entry+=bstr(0x80,1)
        else:
            entry+=bstr(0x00,1)
        mod_len = int(public_key.key_size/8)
        pub_numbers = public_key.public_numbers()
        mod = pub_numbers.n
        exp = pub_numbers.e
        exp_len = exp.bit_length()
        exp_len = int(exp_len/8) if exp_len%8==0 else int(exp_len/8)+1
        rr = 2**(2*public_key.key_size) % pub_numbers.n
        rr_len = rr.bit_length()
        rr_len = int(rr_len/8) if rr_len%8==0 else int(rr_len/8)+1
        entry+= bstr(mod_len,2)+bstr(exp_len,2)+bstr(rr_len,2)+bstr(mod,mod_len)+bstr(exp,exp_len)+bstr(rr,rr_len)
        entry = bstr(0x04,1)+bstr(len(entry)+3,2)+entry
        return entry

    def write_sym_key(self,dek,filename):
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=b'abcdefgh',
            iterations=100000,
            backend=default_backend())
        key = base64.urlsafe_b64encode(kdf.derive(self.password))
        f = Fernet(key)
        token = f.encrypt(dek)
        with open(filename,"wb") as keyfile:
            keyfile.write(token)

    def read_sym_key(self,filename):
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=b'abcdefgh',
            iterations=100000,
            backend=default_backend())
        key = base64.urlsafe_b64encode(kdf.derive(self.password))
        f = Fernet(key)
        with open(filename,"rb") as keyfile:
            return f.decrypt(keyfile.read())


    @staticmethod
    def hash(message):
        hasher = hashes.Hash(hashes.SHA256(),backend=default_backend())
        if isinstance(message,list):
            for entry in message:
                hasher.update(bytes(entry))
        else:
            hasher.update(message)
        hashstr = hasher.finalize()
        return hashstr

    def sign(self,name,message):
        private_key = self.__load_private_key(name)
        digest = self.hash(message)
        sig = private_key.sign(digest,padding.PKCS1v15(),utils.Prehashed(hashes.SHA256()))
        return sig

    def gen_rsa_key(self,name,ca,**kwargs):
        ca_flag=kwargs.get('ca_flag',False)
        size=kwargs.get('size',2048)
        public_exponent=kwargs.get('exp',65537)
        years=kwargs.get('years',10)
        subject=kwargs.get('org',"DEFAULT")
        self.db[name]={}
        private_key = rsa.generate_private_key(key_size=size,public_exponent=public_exponent, backend=default_backend())
        utf8_pass = self.password
        algorithm = serialization.BestAvailableEncryption(utf8_pass)
        filename = os.path.join(self.key_dir,name+'_key.pem')
        with open(filename, "wb") as keyfile:
            keyfile.write(private_key.private_bytes(encoding=serialization.Encoding.PEM,format=serialization.PrivateFormat.PKCS8,encryption_algorithm=algorithm))
        self.db[name]['key']=filename
        self.db[name]['ca']=ca
        cert_subj = x509.Name([x509.NameAttribute(NameOID.ORGANIZATION_NAME, subject)])
        if name == ca:
            issuer = cert_subj
        else:
            certfile = self.__load_public_cert(ca)
            issuer = certfile.subject
        valid_from = datetime.utcnow()
        valid_to = valid_from + timedelta(days=years*365)
        number = x509.random_serial_number()
        public_key = private_key.public_key()
        builder = (x509.CertificateBuilder().subject_name(cert_subj).issuer_name(issuer).public_key(public_key).serial_number(number).not_valid_before(valid_from).not_valid_after(valid_to))
        builder = builder.add_extension(x509.BasicConstraints(ca=ca_flag, path_length= 1 if ca_flag else None), critical=True)
        public_cert = builder.sign(self.__load_private_key(ca), hashes.SHA256(), default_backend())
        filename = os.path.join(self.crt_dir,name+'_crt.pem')
        with open(filename, "wb") as certfile:
            certfile.write(public_cert.public_bytes(serialization.Encoding.PEM))
        self.db[name]['crt']=filename
        self.db[name]['sl']=number

    def gen_hcrt(self,name):
        entry = self.__pub_key_entry(name)
        sig_header = bstr(0x03,1) + bstr(0x00,2)
        sign = self.sign(self.db[name]['ca'],entry+sig_header)
        sig_header = bstr(0x03,1) + bstr(len(sign),2)
        sign = self.sign(self.db[name]['ca'],entry+sig_header)
        filename = self.db[name]['crt'].replace('_crt.pem','_hcrt.bin')
        with open(filename,"wb") as fd:
            fd.write(entry+sig_header+sign)
        self.db[name]['hcrt']=filename

    def get_hcrt(self,name):
        if not 'hcrt' in self.db[name]:
            self.gen_hcrt(name)
        with open(self.db[name]['hcrt'],"rb") as fd:
            return fd.read()

    def __load_public_cert(self,name):
        with open(self.db[name]['crt'],"rb") as certfile:
            return x509.load_pem_x509_certificate((certfile.read()),backend = default_backend())

    def __load_private_key(self,name):
        with open(self.db[name]['key'],"rb") as keyfile:
            return serialization.load_pem_private_key((keyfile.read()),self.password,backend = default_backend())

    def __del__(self):
        with open(self.db_file,"w") as db_file:
            cst_utils.yaml.dump(self.db,db_file)

state = None
