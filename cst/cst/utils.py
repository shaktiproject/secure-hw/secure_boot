from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend
import ruamel
from ruamel.yaml import YAML
import argparse
import logging
import lief

logger = logging.getLogger(__name__)

yaml = YAML(typ="rt")
yaml.default_flow_style = False
yaml.allow_unicode = True

def hash(message):
    hasher = hashes.Hash(hashes.SHA256(),backend=default_backend())
    hasher.update(message)
    digest = hasher.finalize()
    return digest

def load_yaml(foo):
    try:
        with open(foo, "r") as file:
            return yaml.load(file)
    except ruamel.yaml.constructor.DuplicateKeyError as msg:
        logger = logging.getLogger(__name__)
        error = "\n".join(str(msg).split("\n")[2:-7])
        logger.error(error)
        raise SystemExit
    except FileNotFoundError:
        return {}

class ELF():
    def __init__(self, config: dict):
        self.name = config['File']
        self.output = config['Output']
        self.modify = config.get('Modify',True)
        addr = config.get('Address', 0)
        self.address = int(addr,base=16) if isinstance(addr,str) else addr
        self.load = config.get('Load',False)
        self.encrypt = False
        self.binary = lief.parse(self.name)

    def get_data(self,addr_start,length):
        val = self.binary.get_content_from_virtual_address(addr_start,length)
        return bytes(val)

    def encrypt_data(self, blocks, encryptor):
        self.encrypt = True
        for addr, size in blocks:
            if size % 128 != 0:
                print("Incompatible Block Size")
                raise SystemExit
            for segment in self.binary.segments:
                if segment.virtual_address >= addr and \
                    segment.virtual_address + segment.physical_size >= addr+size:
                    offset = addr - segment.virtual_address
                    segment.content = segment.content[:offset]+list(encryptor.update(bytes(segment.content[offset:offset+size])))+segment.content[offset+size:]
    def write_csf(self,csf):
        if self.modify:
            binary = self.binary
            sec = lief.ELF.Section(".csf",lief.ELF.SECTION_TYPES.NOTE)
            sec.name = ".csf"
            sec.type = lief.ELF.SECTION_TYPES.NOTE
            sec.alignment = 8
            sec.content = [int(x) for x in csf]
            sec.entry_size = len(sec.content)
            if self.load:
                if self.address > 0:
                    sec.virtual_address = self.address
                else:
                    sec.virtual_address = binary.next_virtual_address
                sec.add(lief.ELF.SECTION_FLAGS.ALLOC)
                sec = binary.add(sec)
            else:
                sec = binary.add(sec,False)
        else:
            with open(self.output,'wb') as f:
                f.write(csf)
    def __del__(self):
        if self.modify or self.encrypt:
            self.binary.write(self.output if self.modify else self.name)

class ColoredFormatter(logging.Formatter):
    """
        Class to create a log output which is colored based on level.
    """

    def __init__(self, *args, **kwargs):
        super(ColoredFormatter, self).__init__(*args, **kwargs)
        self.colors = {
            'DEBUG': '\033[94m',
            'INFO': '\033[92m',
            'WARNING': '\033[93m',
            'ERROR': '\033[91m',
        }

        self.reset = '\033[0m'

    def format(self, record):
        msg = str(record.msg)
        level_name = str(record.levelname)
        name = str(record.name)
        color_prefix = self.colors[level_name]
        return '{0}{1:<9s} : {2}{3}'.format(color_prefix,
                                            '[' + level_name + ']', msg,
                                            self.reset)

def setup_logging(log_level):
    """Setup logging

        Verbosity decided on user input

        :param log_level: User defined log level

        :type log_level: str
    """
    numeric_level = getattr(logging, log_level.upper(), None)

    if not isinstance(numeric_level, int):
        print(
            "\033[91mInvalid log level passed. Please select from debug | info | warning | error\033[0m"
        )
        raise ValueError("{}-Invalid log level.".format(log_level))

    logging.basicConfig(level=numeric_level)

