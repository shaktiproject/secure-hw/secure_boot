import logging,os
import base64
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from collections import OrderedDict
from cst.state import bstr
import cst.utils as utils
def generate_csf(state,csf_file,xlen):
    logger = logging.getLogger(__name__)
    logger.info("Generating CSF Binary")
    csf_input = utils.load_yaml(csf_file)
    csf_dict = {}
    csf = OrderedDict()
    data = OrderedDict()
    size = 0
    srk_idx = 0
    signk_idx = 2
    signk_name = ''
    interface = None
    data_iv = None
    blocks = []
    dec_blocks = []
    addr_width = int(xlen/8)
    for entry in csf_input:
        csf_dict[entry['Type']] = entry
        if(entry['Type']=='Header'):
            size = size + 5
        elif(entry['Type']=='Authenticate CSF'):
            size += 9 if 'Install Secret Key' not in csf_dict else 17
        elif(entry['Type']=='Install Key' or entry['Type'] == 'Install SRK' or entry['Type'] == 'Install Secret Key'):
            size = size + 10
        elif(entry['Type']=='Authenticate Data'):
            block_input = entry['Blocks']
            chain = entry.get('Chain',False)
            for block in block_input:
                temp = block.split(' ')
                mem_address_start = int(str(temp[0]),16)
                offset = int(temp[1],16)
                data_length = int(temp[2],16)
                blocks.append([mem_address_start+offset,data_length])
            if not chain:
                size = size + len(blocks) * (9+2*addr_width)
            else:
                size += 9+len(blocks)*2*addr_width
        elif(entry['Type']=='Decrypt Data'):
            block_input = entry['Blocks']
            for block in block_input:
                temp = block.split(' ')
                mem_address_start = int(str(temp[0]),16)
                offset = int(temp[1],16)
                data_length = int(temp[2],16)
                dec_blocks.append([mem_address_start+offset,data_length])
            size += 9+len(dec_blocks)*2*addr_width
            size += 8 #account for key blob offset and length in auth csf command
    tot_size = size
    if 'Header' in csf_dict:
        csf_ver = csf_dict['Header']['CSF Version']
        csf_ver_val = int(10*csf_ver)
        prog_version =  int(csf_dict['Header']['Program Version'])
        if prog_version > 255 or prog_version not in [0,1,3,7,31,63,127,255]:
            logger.error("Wrong program version. Choose from 0,1,3,7,31,63,127,255.")
        csf['Header']=[(0x0f,1),(size,2),(csf_ver_val,1),(prog_version,1)]
    else:
        logger.error("No Header details found.")
        raise SystemExit
    if 'Install SRK' in csf_dict:
        tag = 0x10
        length = 10
        hash_alg = csf_dict['Header']['Hash Algorithm']
        hash_algo = 0x1
        if(hash_alg=="sha256"):
            hash_algo = 0x1 # 1 - sha256, 0 - None
        else:
            hash_algo = 0x0
        crypto_algo = 0x0f
        type_of_key = 0x1
        key_verif_idx = csf_dict['Install SRK']['Source index']
        srk_idx = csf_dict['Install SRK'].get('Target index',0)
        srk_files = csf_dict['Install SRK']['Name'].split(' ')
        lines = b''
        table, fuse = state.gen_srk_table(srk_files)
        with open(csf_dict['Install SRK']['Fuse'],"rb") as fuse_fd:
            fuse_val = fuse_fd.read()
        if fuse == fuse_val:
            logger.debug("SRK Fuse value matches.")
        else:
            logger.error("Invalid SRK Fuse.")
            raise SystemExit
        data['Install SRK'] = table
        csf['Install SRK'] = [(tag,1), (length,1),(type_of_key,1),(crypto_algo,1),(key_verif_idx,1),(srk_idx,1),(tot_size,4)]
        tot_size = tot_size+len(table)
    if 'Install Key' in csf_dict:
        tag = 0x10
        length=10
        type_of_key = 0x2
        key_verif_idx = csf_dict['Install Key'].get('Verification index',srk_idx)
        signk_idx = csf_dict['Install Key'].get('Target Index',signk_idx)
        signk_name = csf_dict['Install Key']['Name']
        imgcrt = state.get_hcrt(signk_name)
        imgk_size = len(imgcrt)
        data['Install Key'] = bstr(0x02,1) + bstr(imgk_size,2) + imgcrt
        csf['Install Key']=[(tag,1),(length,1),(type_of_key,1),(crypto_algo,1),(key_verif_idx,1),(signk_idx,1),(tot_size,4)]
        tot_size = tot_size+imgk_size+3
    else:
        logger.error("No Install Key command found.")
        raise SystemExit
    if 'Authenticate CSF' in csf_dict:
        tag = 0x11
        hash_alg = csf_dict['Header']['Hash Algorithm']
        if(hash_alg=='sha256'):
            hash_algo = 0x1
        else:
            hash_algo = 0x0
        crypto_algo = 0x0f
        length = 9 if 'Install Secret Key' not in csf_dict else 17
        key_verif_idx = csf_dict['Authenticate CSF'].get('Verification index',signk_idx)
        data['Authenticate CSF'] = bstr(0x03,1)
        csf['Authenticate CSF'] = [(tag,1),(length,1),(hash_algo,1),(crypto_algo,1),(key_verif_idx,1),(tot_size,4)]
        tot_size = tot_size+3
    else:
        logger.error("No Authenticate CSF command found.")
        raise SystemExit
    if 'Authenticate Data' in csf_dict:
        tag = 0x12
        hash_alg = csf.get('Header','Hash Algorithm')
        if(hash_alg=='sha256'):
            hash_algo = 0x1 # 1 - sha256, 0 - None
        else:
            hash_algo = 0x0
        crypto_algo = 0x0f
        key_verif_idx = csf_dict['Authenticate Data'].get('Verification index',signk_idx)
        interface = utils.ELF(dict(csf_dict['Authenticate Data']))
        if 'Decrypt Data' in csf_dict:
            node = dict(csf_dict['Decrypt Data'])
            data_iv = os.urandom(16)
            name = node['Name']
            key = state.read_sym_key(state.db[name]['key'])
            if node['Mode'] == 'CBC':
                cipher = Cipher(algorithms.AES(key), modes.CBC(data_iv),default_backend())
            elif node['Mode'] == 'CTR':
                cipher = Cipher(algorithms.AES(key), modes.CTR(data_iv),default_backend())
            elif node['Mode'] == 'ECB':
                cipher = Cipher(algorithms.AES(key), modes.ECB(),default_backend())
            encryptor = cipher.encryptor()
            interface.encrypt_data(dec_blocks,encryptor)
        sign_len = len(state.sign(signk_name,b''))
        tot_size+=sign_len
        signatures = b''
        if chain:
            command = [(tag,1), (9+len(blocks)*2*addr_width,1),(hash_algo,1),(crypto_algo,1),(key_verif_idx,1)]
            img_data = []
            command += [(tot_size,4)]
            for block in blocks:
                img_data.append(interface.get_data(block[0],block[1]))
                command+=[(block[0],addr_width),(block[1],addr_width)]
            csf['Authenticate Data'] = [command]
            data['Authenticate Data'] = bstr(0x03,1)+bstr(sign_len,2)+state.sign(signk_name,img_data)
            tot_size+=sign_len+3
        else:
            csf['Authenticate Data'] = []
            command = [(tag,1), (9+2*(addr_width),1),(hash_algo,1),(crypto_algo,1),(key_verif_idx,1)]
            for block in blocks:
                img_sign = state.sign(signk_name,interface.get_data(block[0],block[1]))
                csf['Authenticate Data'].append(command + [(tot_size,4),(block[0],addr_width),(block[1],addr_width)])
                tot_size+=sign_len+3
                signatures+= bstr(0x03,1)+bstr(sign_len,2)+img_sign
            data['Authenticate Data'] = signatures
    else:
        logger.error("No Authenticate Data command found.")
        raise SystemExit

    if 'Install Secret Key' in csf_dict:
        tag = 0x10
        length=10
        type_of_key = 0x3
        crypto_algo = 0x01
        key_verif_idx = csf_dict['Install Secret Key'].get('Verification index',0)
        seck_idx = csf_dict['Install Secret Key'].get('Target Index',0)
        seck_name = csf_dict['Install Secret Key']['Name']
        with open(state.db[seck_name]['crt'],"rb") as crtfile:
            enc_blob = crtfile.read()
        seck_size = len(enc_blob)
        data['Install Secret Key'] = bstr(0x05,1) + bstr(seck_size,2) + enc_blob
        csf['Authenticate CSF'] += [(tot_size,4),(seck_size+3,4)]
        csf['Install Secret Key'] = [(tag,1),(length,1),(type_of_key,1),(crypto_algo,1),(key_verif_idx,1),(signk_idx,1),(tot_size,4)]
        tot_size = tot_size+seck_size+3

    if 'Decrypt Data' in csf_dict:
        tag = 0x13
        mode = csf_dict['Decrypt Data']['Mode']
        if(mode=='CBC'):
            mode = 0x1
        elif(mode == 'CTR'):
            mode = 0x2
        elif(mode == 'ECB'):
            mode = 0x3
        else:
            mode = 0x0
        crypto_algo = 0x01
        key_idx = csf_dict['Decrypt Data'].get('Key index',seck_idx)
        command = [(tag,1), (9+len(dec_blocks)*2*addr_width,1),(crypto_algo,1),(mode,1),(key_idx,1),(tot_size,4)]
        for block in dec_blocks:
            command += [(block[0],addr_width),(block[1],addr_width)]
        csf['Decrypt Data']=command
        data['Decrypt Data'] = data_iv
        tot_size += 16

    csf_bstr = b''
    for entry in csf:
        if entry == 'Authenticate Data':
            for command in csf[entry]:
                command_bstr=b''
                for field in command:
                    command_bstr+=bstr(field[0],field[1])
                csf_bstr+=command_bstr
        else:
            command_bstr=b''
            for field in csf[entry]:
                command_bstr+=bstr(field[0],field[1])
            csf_bstr+=command_bstr
    if 'Install Secret Key' not in csf:
        csf_sign = state.sign(signk_name,csf_bstr)
    else:
        csf_sign = state.sign(signk_name,csf_bstr+data['Install Secret Key'])
    data['Authenticate CSF'] += bstr(len(csf_sign),2)+csf_sign
    data_bstr = b''
    for key, value in data.items():
        data_bstr+=value
    interface.write_csf(csf_bstr+data_bstr)
