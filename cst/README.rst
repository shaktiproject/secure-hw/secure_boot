**NOTE: Ensure that you have a python venv set up before following the steps for development.**

To install dependencies and get started, run the following command in a venv.
``` shell

    pip install --editable .

```

To generate a CSF using cst do the following.

``` shell

    $ mkdir work
    $ cd work
    $ cst init
    $ cd ..
    $ cst -cf ./work/config.ini gen-csf --csf-file examples/csf_secure_boot2.ini


```
